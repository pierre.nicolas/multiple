#!/usr/bin/perl -w
use strict;

my $usagemsg = "Usage: extract_palindromic_structure.pl <PWM output file> [-wmax wmax (default 25)]\n 
\twmax needs to be consistent with the parameter of the Multiple command line\n";
(@ARGV>=1) || die $usagemsg;

    
my $inputfile = shift @ARGV;
print "Extracting palindromic structure out of file: '", $inputfile, "'", "\n";

my $wmax=25;
while (@ARGV>0) {
    my $nextarg = shift @ARGV;
    if ($nextarg eq "-wmax" && @ARGV>=1) {
	$wmax = shift @ARGV;
	print "wmax set to ", $wmax, "\n";
    } else {
	die $usagemsg;
    }
}

($inputfile =~ /_PWM.txt$/) || die "expecting filename terminated by _PWM.txt";

my $outputfile = $inputfile;
$outputfile =~ s/_PWM.txt$/_palindrome.txt/;

my @palcol;
for (my $i=0; $i<25; $i++) {
    push @palcol, "col".($i+1);
}
open(IN, "<$inputfile") || die "cannot open ".$inputfile;
open(OUT, ">$outputfile") || die "cannot open ".$outputfile;
print OUT "sweep", " ", "motif", " ", "w", " ", "centerpal", " ", "npairedcol_pal", " ", join(" ", @palcol), "\n";
while (my $line = <IN>) {
    chomp($line);
    if ($line =~ /^sweep/) {
	#if ($line =~ /^sweep = (\d+) motif = (\d+) w = (\d+) refpos = (\d+) centerpal = (-?\d+) npairedcol_pal = (\d+)/) {
	if ($line =~ /^sweep = (\d+) motif = (\d+) w = (\d+) refpos = (\d+) centerpal = ([\-\.\d]+) npairedcol\_pal = (\d+)/) {
	    my $isweep = $1;
	    my $imotif = $2;
	    my $wmotif = $3;
	    my $refpos = $4;
	    my $centerpal = $5;
	    my $npairedcol = $6;
	    my @pal;
	    $line = <IN>;
	    while ($line =~ /^([\-\d]+)\s+/) {
		push @pal, $1;
		$line = <IN>;
		chomp($line);
	    }
	    while (@pal<25) {
		push @pal, "NA";
	    }
	    print OUT $isweep, " ", $imotif, " ", $wmotif, " ", $centerpal, " ", $npairedcol, " ", join(" ", @pal), "\n";
	    #die;
	} else {
	    die "cannot parse line: '",$line, "'\n"; 
	}
    }
}
close(OUT);
close(IN);
