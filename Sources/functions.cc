/*
    name : functions.cc

    This file is part of the source code of Multiple -- a program for
    de novo discovery of DNA motifs corresponding to Transcription
    Factor binding sites based on a probabilistic model of the DNA
    sequence that incorporates expression data as covariates.

    Copyright (C) 2018  Ibrahim Sultan

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING . If not, write to the
    Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Please send bugreports with examples or suggestions to pierre.nicolas@inra.fr
*/


#include "functions.h"
    
using namespace std;


void Functions::initialize_prior_centerpal(Para& para, double alpha_centerpal)
{
  cout << "entering initialize_prior_centerpal" << endl;
  para.prior_centerpal = new double* [para.Wmax+1];
  for (int w=2*para.min_halfpal; w<=para.Wmax; w++) {
    para.prior_centerpal[w] = new double [2*w+1];
    double tmid_motif = (double)w/2.0;
    double tsum = 0;
    for (int tcenter=0; tcenter<2*w+1; tcenter++) {
      para.prior_centerpal[w][tcenter]=0;
      if (tcenter>=2*para.min_halfpal && tcenter <= 2*w-2*para.min_halfpal) {
	double tdist = 0;
	if ((double)tcenter/2.0>tmid_motif) {
	  tdist = (double)tcenter/2.0 - (tmid_motif+1);
	} else if ((double)tcenter/2.0<tmid_motif) {
	  tdist = (tmid_motif-1) - (double)tcenter/2.0;
	}
	if (tdist<0) {
	  tdist = 0;
	}
	if (tdist<0) {
	  tdist *= -1;
	}
	para.prior_centerpal[w][tcenter]= pow(alpha_centerpal,tdist); //2^tdist seems a very strong penalization
	tsum += para.prior_centerpal[w][tcenter];
      }
    }
    for (int tcenter=0; tcenter<2*w+1; tcenter++) {
      para.prior_centerpal[w][tcenter] /= tsum;
    }
    cout << "w=" << w << " : ";
    for (int tcenter=0; tcenter<2*w+1; tcenter++) {
      cout << " " << para.prior_centerpal[w][tcenter];
    }
	cout << endl;
  }
}


int Functions::sample(double* pr, gsl_rng* p, double sum) {
    double _r=gsl_rng_uniform(p)*sum;
    int ri=0;
    double tsum = pr[ri];
    while (_r > tsum) {
        ri++;
        tsum+=pr[ri];
    }
    return(ri);
}

    //==================begin number of regions===============

void Functions::Matrix_change(Para Para, int** position, int** pos_counter) {
    int M=Para.M;
    int L=Para.L;
    for(int m=0; m<M; m++) {
        for(int l=0; l<L; l++) {
            pos_counter[m][l]=0;
        }
        double counter_sum = 0;
        for(int s=0; s<Para.S; s++) {
            if (0<=position[m][s] && position[m][s]<L) {
                pos_counter[m][position[m][s]]++;
                counter_sum++;
            }
        }
    }
}

void Functions::count_Q (int* K,int** D, double** Q,Para Para, int** pos_counter) {
    for (int m=0; m<Para.M; m++) {
        count_Q_onemotif (Para, pos_counter[m], D[m], Q[m], K[m]);
    }
}

void Functions::count_Q_onemotif (Para Para, int* pos_counter_motif, int* D_motif, double* Q_motif, int km) {
    fill_n (Q_motif, Para.P+1, 0); //prior for number of occuerence in each region
    int k=0;
    for (int l=0; l< Para.L; l++) {
        Q_motif[k] += pos_counter_motif[l];
        if (l<Para.L-1 && D_motif[l]==1) {
            k++;
        }
    }
}

void Functions::update_Gamma(int** D, double** Q, double** Gamma, Para Para, gsl_rng* p, int* K) { //probability for each region (\Gamma), given Q & K
    for (int m=0; m<Para.M; m++) {
        if (K[m]>1) {
            double testo = 0;
            double Gamma_sum = 0;
            double tempo_Q[K[m]];
            double temp_Gamma[K[m]];
            for (int k=0; k<K[m]; k++) {
                tempo_Q[k]= Q[m][k]+Para.z1; //Para.z //sub_regions
                testo += Q[m][k];
            }
            gsl_ran_dirichlet (p, K[m], tempo_Q, temp_Gamma);
            for (int t=0; t<K[m]; t++) {
                Gamma[m][t] = temp_Gamma[t];
                Gamma_sum += Gamma[m][t];
            }
        } else {
            Gamma[m][0]=1;
        }
        for (int k=K[m]; k<Para.P+1; k++) {
            Gamma[m][k]=0;
        }
    }
}

void Functions::cal_pos_prob (int** D, int* K, double** Gamma, Para& Para, double** pos_prob, int** point_detector) { //probability for each position, given \Gamma & D
    for (int m=0; m<Para.M; m++) {
        double sum_gamma = 0;
        int sum_dis;
        int dis_region [Para.P+1];
        fill_n (dis_region, Para.P+1, 0);
        fill_n (point_detector[m], Para.P, 0);
        double sum_pos_prob=0;
        if (K[m]>1) {
            int k = 1;
            for (int l=0; l<Para.L-1; l++) {
                if (D[m][l]==1) {
                    point_detector[m][k-1]=l;
                    k++;
                }
            }
            sum_pos_prob=0;
            dis_region[0] = point_detector[m][0]+1;
            dis_region[K[m]-1]=(Para.L-1)-point_detector[m][K[m]-2];
            sum_dis = dis_region[0]+dis_region[K[m]-1];//display
            for (int k=1; k<K[m]-1; k++) {
                dis_region[k]=point_detector[m][k]-point_detector[m][k-1];
                sum_dis += dis_region[k];//display
            }
            k = 0;
            sum_gamma = Gamma[m][0];
            for (int l=0; l<Para.L; l++) {
                pos_prob[m][l] = Gamma[m][k]/(double)dis_region[k];
                if (l<Para.L-1 && D[m][l]==1) { // PN added l<Para.L-1
                    k++;
                    sum_gamma += Gamma[m][k];
                }
                sum_pos_prob += pos_prob[m][l];
            }
        } else {
            for (int l=0; l<Para.L; l++) {
                pos_prob[m][l] = 1.0/(double)Para.L;
            }
        }
    }
}

void Functions::sample_D (int** D, int** temp_D, Para Para, int* K, gsl_rng* p, double** Q, double** temp_Q, int** pos_counter, int** point_detector) {
    for (int m=0; m<Para.M; m++) {
        count_Q_onemotif (Para, pos_counter[m], D[m], Q[m], K[m]);
        if (K[m]>1 && K[m]<Para.P+1) {
            int point_detector_temp [Para.P];
            fill_n(point_detector[m], Para.P, 0);
            fill_n(point_detector_temp, Para.P, 0);
            for (int l=0; l<Para.L-1; l++) {
                temp_D[m][l]=D[m][l];
            }
            int k=1;
            for (int t=0; t<Para.L-1; t++) {
                if (D[m][t]==1) {
                    point_detector[m][k-1]=t;
                    k++;
                }
            }
            int d;
            d=(int)gsl_rng_uniform_int(p, Para.L-1);
            while(temp_D[m][d]==0) {
                d=(int)gsl_rng_uniform_int(p, Para.L-1);
            }
            temp_D[m][d]=0;    //remove bp
            d=(int)gsl_rng_uniform_int(p, Para.L-1);
            while (temp_D[m][d]==1 || d<10 || d>Para.L-10-1) {
                d=(int)gsl_rng_uniform_int(p, Para.L-1);
            }
            temp_D[m][d]=1;    //add bp
            k=1;
            for (int t=0; t<Para.L-1; t++) {
                if (temp_D[m][t]==1) {
                    point_detector_temp[k-1]=t;
                    k++;
                }
            }
            
            count_Q_onemotif (Para, pos_counter[m], temp_D[m], temp_Q[m], K[m]);
            
            double sum1 = (log(point_detector[m][0]+1)*(Q[m][0])) + (log((Para.L-1)-point_detector[m][K[m]-2])*(Q[m][K[m]-1]));
            double sum2 = (log(point_detector_temp[0]+1)*(temp_Q[m][0]))+(log((Para.L-1)-point_detector_temp[K[m]-2])*(temp_Q[m][K[m]-1]));
            double sum3 = 0;
            double sum4 = 0;
            for (int k=1; k<K[m]-1; k++) {
                sum1 += (log(point_detector[m][k]-point_detector[m][k-1])*(Q[m][k]));
                sum2 += (log(point_detector_temp[k]-point_detector_temp[k-1])*(temp_Q[m][k]));
            }
            
            for (int k=0; k<K[m]; k++) {
                sum3 += gsl_sf_lngamma (temp_Q[m][k]+Para.z1); //Para.z //sub_regions
                sum4 += gsl_sf_lngamma (Q[m][k]+Para.z1);//Para.z //sub_regions
            }
            
            double choose=gsl_rng_uniform(p);
            double test = sum1-sum2+sum3-sum4;
            if (log(choose) < test) {
                for (int k=0; k<K[m]; k++) {
                    Q[m][k]=temp_Q[m][k];
                }
                for (int tt=0; tt<Para.L-1; tt++) {
                    D[m][tt]=temp_D[m][tt];
                }
                for (int k=0; k<K[m]-1; k++) {
                    point_detector[m][k]=point_detector_temp[k];
                }
            }
        }
    }
}

void Functions::update_K (int** D, int** temp_D, Para& Para, int* K, gsl_rng* p, double** Q, double** temp_Q, int** pos_counter, int** point_detector, double geo_prior_breakpoints) {
    int L = Para.L;
    int M = Para.M;
    int scenario;
    for (int m=0; m<M; m++) {
            count_Q_onemotif (Para, pos_counter[m], D[m], Q[m], K[m]);
        double rani = gsl_rng_uniform(p);
        if ( (rani<0.5 && K[m]>1) || (rani>=0.5 && K[m]<Para.P+1) ) {
            int K_temp;
            double Q_tot = 0;
                //int point_detector[Para.P];
            fill_n(point_detector[m], Para.P, 0);
            for (int l=0; l<L; l++) {
                Q_tot += pos_counter[m][l];
            }
            int k=1;
            for (int l=0; l<L-1; l++) {
                temp_D[m][l]=D[m][l];
                if (D[m][l]==1) {
                    point_detector[m][k-1]=l;
                    k++;
                }
            }
            if (rani < 0.5) {//decrease
                scenario = 1;
                int d=(int)gsl_rng_uniform_int(p, L-1);
                while(temp_D[m][d]==0) {
                    d=(int)gsl_rng_uniform_int(p, L-1);
                }
                temp_D[m][d] =0;
                K_temp = K[m]-1;
            } else {//increase
                scenario = 2;
                int d = (int)gsl_rng_uniform_int(p, L-1);
                while (temp_D[m][d]==1 || d<10 || d>Para.L-10-1) { //Here //Remember!
                    d = (int)gsl_rng_uniform_int(p, L-1);
                }
                temp_D[m][d] =1;
                K_temp = K[m]+1;
            }
            int point_detector_temp[Para.P];
            fill_n (point_detector_temp, Para.P, 0);
            k = 1;
            for (int t=0; t<L-1; t++) {
                if (temp_D[m][t]==1) {
                    point_detector_temp[k-1]=t;
                    k++;
                }
            }
            count_Q_onemotif (Para, pos_counter[m], temp_D[m], temp_Q[m], K_temp);
            double sum9, sum10, sum11, sum12, sum13;
            sum9 = gsl_sf_lngamma (Q_tot+(double)K[m]*Para.z1); //Para.z //sub_regions
            sum10 = gsl_sf_lngamma (Q_tot+(double)K_temp*Para.z1); //Para.z //sub_regions
            sum11 = gsl_sf_lngamma (K_temp*Para.z1) - (double)K_temp*gsl_sf_lngamma(Para.z1);
            sum12 = gsl_sf_lngamma (K[m]*Para.z1) - (double)K[m]*gsl_sf_lngamma(Para.z1);
            if (scenario==1) {//decrease //K'= K-1
                sum13 = (double)K[m]/(L-1-K_temp);
            } else if (scenario==2) {
                sum13 = (double)(L-1-K[m])/K_temp;
            }
            double Geometric_prior = (K_temp-K[m])*log(geo_prior_breakpoints);
            //double Geometric_prior = 0; //remember for the uniform prior
            sum13 = log(sum13); //Proposal
            double sum1 = 0;
            double sum2 = 0;
            double sum2_check = 0;
            if (K[m]==1) {
                sum1 = log(Para.L)*(Q_tot);
            } else {
                sum1 = (log(point_detector[m][0]+1)*(Q[m][0])) + (log((Para.L-1)-point_detector[m][K[m]-2])*(Q[m][K[m]-1])); // PN problem here withK[m]-2  when K[m]==1
            }
            if (K_temp==1) {
                sum2 = log(Para.L)*(Q_tot);
                sum2_check = Para.L; // PN added
            } else {
                sum2 = (log(point_detector_temp[0]+1)*(temp_Q[m][0])) + (log((L-1)-point_detector_temp[K_temp-2]))*(temp_Q[m][K_temp-1]);
                sum2_check = (point_detector_temp[0]+1)+ ((Para.L-1)-point_detector_temp[K_temp-2]);
            }
            double sum3 = 0;
            double sum4 = 0;
            for (int k=1; k<K[m]-1; k++) {
                sum1 += (log(point_detector[m][k]-point_detector[m][k-1])*(Q[m][k]));
            }
            for (int k=0; k<K[m]; k++) {
                sum4 += gsl_sf_lngamma (Q[m][k]+Para.z1); //Para.z //sub_regions
            }
            for (int k=1; k<K_temp-1; k++) {
                sum2 += (log(point_detector_temp[k]-point_detector_temp[k-1])*(temp_Q[m][k]));
                sum2_check += (point_detector_temp[k]-point_detector_temp[k-1]);
            }
            for (int k=0; k<K_temp; k++) {
                sum3 += gsl_sf_lngamma (temp_Q[m][k]+Para.z1); //para.z //sub_regions
            }
            double sum5 = gsl_sf_lnfact (K[m]);
            double sum6 = gsl_sf_lnfact (K_temp);
            double sum7 = gsl_sf_lnfact (L-1-K[m]);
            double sum8 = gsl_sf_lnfact (L-1-K_temp);
            double choose = gsl_rng_uniform(p);
            double test = sum1-sum2+sum3-sum4-sum5+sum6-sum7+sum8+sum9-sum10+sum11-sum12+sum13+Geometric_prior;
            if (log(choose) < test) {
                Para.Naccept++;
                K[m]=K_temp;
                for (int k=0; k<K[m]; k++) {
                    Q[m][k]=temp_Q[m][k];
                }
                for (int tt=0; tt<Para.L-1; tt++) {
                    D[m][tt]=temp_D[m][tt];
                }
                for (int k=0; k<K[m]-1; k++) {
                    point_detector[m][k]=point_detector_temp[k];
                }
            }
        }
    }
}

    //==================end number of regions===============

    //==================begin nucleotide composition========
void Functions::count_theta (int*** motifs, int** seq, int** pos, int** B, Para Para, int* w, int* refpos) {
    for (int m=0; m<Para.M; m++) {
        for (int i=0; i<w[m]; i++) {
            for (int j=0; j<4; j++) {
                motifs[m][i][j] = 0;
            }
        }
        for (int s=0; s<Para.S; s++) {
            for (int r=0; r<w[m]; r++) {
                if (pos[m][s]-refpos[m]+r < Para.L) {
                    int iposinseq = pos[m][s]-refpos[m]+r;
                    if (0 <= iposinseq && iposinseq < Para.L && B[s][iposinseq] == m) {
                        int xi = seq[s][iposinseq];
                        motifs[m][r][xi]++;
                    }
                }
            }
        }
    }
}

double Functions::integrate_dirichlet4_pobs(double alpha, int* countobs)
{
  double ret_val = gsl_sf_lngamma(4.0*alpha) - 4*gsl_sf_lngamma(alpha);
  double tsum = 0;
  for (int a=0; a<4; a++) {
    double tt=(double)countobs[a]+alpha;
    ret_val += gsl_sf_lngamma(tt);
    tsum += tt;
  }
  ret_val -= gsl_sf_lngamma(tsum);  
  return(ret_val);
}

double Functions::integrate_dirichlet4_pobs_mergedpal(double alpha, int* countobs1,
						      int* countobs2)
{
  double ret_val = gsl_sf_lngamma(4.0*alpha) - 4*gsl_sf_lngamma(alpha);
  double tsum = 0;
  for (int a=0; a<4; a++) {
    double tt=(double)(countobs1[a]+countobs2[4-a-1])+alpha; //0:A, 1:C, 2:G, 3:T
    ret_val += gsl_sf_lngamma(tt);
    tsum += tt;
  }
  ret_val -= gsl_sf_lngamma(tsum);  
  return(ret_val);
}

void Functions::update_pal(int*** motifs, Para& Para, gsl_rng* p, int* w, int* center_pal, int** pospair_pal) {
  if (!Para.pal_activated) {
    return;
  }
  if (Para.pal_activated && Para.ICbg) {
    cerr << "Error: Unable to allow palindromic motifs when taking into account IC in motif overlap" << endl;
    exit(1);    
  }
  
  double* lnprobacounts_notpal = new double [Para.Wmax];
  for (int m=0; m<Para.M; m++) {
    if (w[m] >= 2*Para.min_halfpal) {
      
      for (int i=0; i<Para.Wmax; i++) {
	if (i<w[m]) {
	  lnprobacounts_notpal[i]=integrate_dirichlet4_pobs(Para.z,motifs[m][i]);
	} else {
	  lnprobacounts_notpal[i]=0;
	}
      }
      
      int npossible_center_pal = 2*(w[m]-Para.min_halfpal)-2*Para.min_halfpal+1;
      //cout << "motif " << m << " width " << w[m] << " npossible_center_pal " << npossible_center_pal << endl;
      //cout << " nocc firstcol " << motifs[m][0][0]+motifs[m][0][1]+motifs[m][0][2]+motifs[m][0][3] << endl;
      double* ratiolik = new double [npossible_center_pal+1];
      ratiolik[npossible_center_pal]=1-Para.ispal_prior; // not a palindromic motif
      int ipossiblepal = 0;
      for (int tcenter_pal = 2*Para.min_halfpal; tcenter_pal<=2*(w[m]-Para.min_halfpal);
	   tcenter_pal++) {
	//cout << ipossiblepal << " tcenter_pal " << tcenter_pal << endl;
	ratiolik[ipossiblepal] = Para.ispal_prior*Para.prior_centerpal[w[m]][tcenter_pal];
	//ratiolik[ipossiblepal] = Para.ispal_prior/(double)npossible_center_pal;
	for (int i=0; 2*i<tcenter_pal; i++) {
	  int ipaired = (tcenter_pal+(tcenter_pal-2*i-2))/2;
	  if (i!=ipaired && ipaired<w[m]) {
	    //cout << " pairing " << i << "<->" << ipaired << endl;
	    //cout << "   " << integrate_dirichlet4_pobs_mergedpal(Para.z,motifs[m][i],motifs[m][ipaired]) << " - " << lnprobacounts_notpal[i] << " - " << lnprobacounts_notpal[ipaired] << endl;
	    ratiolik[ipossiblepal] *= (1-Para.ispaired_inpal_prior)
	      + Para.ispaired_inpal_prior*exp(integrate_dirichlet4_pobs_mergedpal(Para.z,motifs[m][i],motifs[m][ipaired])-lnprobacounts_notpal[i]-lnprobacounts_notpal[ipaired]);
	  }	  
	}
	ipossiblepal++;
      }

      //cout << "ratiolik";
      double tsum=0;
      for (int i=0; i<npossible_center_pal+1; i++) {
        tsum += ratiolik[i];
	//cout << " " << ratiolik[i];
      }
      //cout << endl;
      
      int choose = sample(ratiolik, p, tsum);
      for (int i=0; i<Para.Wmax; i++) {
	pospair_pal[m][i]=-1;
      }
      if (choose == npossible_center_pal) {
	center_pal[m]=-1; //not palindromic
      } else {
	center_pal[m]= 2*Para.min_halfpal + choose;
	//cout << "motif " << m << " width " << w[m] << " center_pal " << (double)center_pal[m]/2.0 << endl;
	for (int i=0; 2*i<center_pal[m]; i++) {
	  int ipaired = (center_pal[m]+(center_pal[m]-2*i-2))/2;
	  if (i!=ipaired && ipaired<w[m]) {
	    //cout << " trying to pair " << i << " with " << ipaired << endl;
	    double p_notpaired = (1-Para.ispaired_inpal_prior);
	    double p_paired = Para.ispaired_inpal_prior*exp(integrate_dirichlet4_pobs_mergedpal(Para.z,motifs[m][i],motifs[m][ipaired])-lnprobacounts_notpal[i]-lnprobacounts_notpal[ipaired]);
	    double x = gsl_rng_uniform(p);
	    if (x<p_paired/(p_paired+p_notpaired)) {
	      //cout << "  -> paired" << endl;
	      pospair_pal[m][i]=ipaired;
	      pospair_pal[m][ipaired]=i;	      
	    }
	  }	  
	}
      }
      delete [] ratiolik;
    }
    
  }
  delete [] lnprobacounts_notpal;
}
  


void Functions::update_theta (int*** motifs, double*** prob, Para Para, gsl_rng* p, int* w, double** IC, double** IC_temp, int** pos, int* refpos, double*** prob_temp, bool ICbg, int** B, int* center_pal, int** pospair_pal) {
    int accepted=0, total=0;
    double *alpha = new double [4];
    double *theta_prop = new double [4];
    if (ICbg) { //Using Information Content
        for (int m=0; m<Para.M; m++) {
	  if (Para.pal_activated) {
	    cerr << "Error: Unable to allow palindromic motifs when taking into account IC in motif overlap" << endl;
	    exit(1);    	    
	  }
            for (int i=0; i<w[m]; i++) {
                for (int j=0; j<4; j++) {
                    alpha[j] = (double)motifs[m][i][j]+ Para.z; //Para.z
                }
                gsl_ran_dirichlet (p, 4, alpha, theta_prop);
                double IC_prop = computeIC(theta_prop);
                double sum1=0;
                double sum2=0;
                for (int n=0; n<Para.S; n++) {
                    int iposinseq=pos[m][n]-refpos[m]+i;
                    if (pos[m][n]<Para.L && iposinseq>=0 && iposinseq<Para.L) {
                        double tsum1_num=0;
                        double tsum2_den=IC_prop;
                        double tsum2_num=0;
                        if (B[n][iposinseq]==m) {
                            tsum1_num=IC[m][i];
                            tsum2_num=IC_prop;
                        } else {
                            int m1=B[n][iposinseq];
                            tsum1_num=IC[m1][iposinseq-pos[m1][n]+refpos[m1]];
                            tsum2_num=tsum1_num;
                        }
                        double tsum1_den=IC[m][i];
                        double tsum_den=0;
                        for (int m1=0; m1<Para.M; m1++) {
                            if (m1!=m && pos[m1][n]-refpos[m1]<=iposinseq && iposinseq<pos[m1][n]-refpos[m1]+w[m1] && pos[m1][n]<Para.L) {
                                tsum_den += IC[m1][iposinseq-pos[m1][n]+refpos[m1]];
                            }
                        }
                        sum1 += log(tsum1_num/(tsum1_den+tsum_den));
                        sum2 += log(tsum2_num/(tsum2_den+tsum_den));
                    }
                }
                double choose = gsl_rng_uniform(p);
                double test = sum2-sum1;
                total++;
                if (log(choose) < test) {
                    accepted++;
                    IC[m][i] = IC_prop;
                    for (int r=0; r<4; r++) {
                        prob[m][i][r] = theta_prop[r];
                    }
                }
            }
        }
        compute_information_content (Para, IC, prob, w);
    } else { //Ignoring Information Content
        for (int m=0; m<Para.M; m++) {
            for (int i=0; i<w[m]; i++) {
	      if (pospair_pal[m][i]<0) {
                for (int r=0; r<4; r++) {
		  alpha[r] = (double)motifs[m][i][r]+ Para.z; //Para.z
                }
                gsl_ran_dirichlet (p, 4, alpha, theta_prop);
                for (int r=0;r<4;r++) {
		  prob[m][i][r]=theta_prop[r];
                }
	      } else {
		int ipaired = pospair_pal[m][i];
		if (pospair_pal[m][ipaired]!=i) {
		  cerr << "Inconsistancy here pospair_pal[m][ipaired]!=i" << endl;
		  exit(1);
		}
		if (i <= ipaired) {
		  for (int r=0;r<4;r++) {
		    alpha[r] = (double)(motifs[m][i][r]+motifs[m][ipaired][4-r-1]) + Para.z; //Para.z
		  }
		  gsl_ran_dirichlet (p, 4, alpha, theta_prop);
		  for (int r=0;r<4;r++) {
		    prob[m][i][r]=theta_prop[r];
		    prob[m][ipaired][r]=theta_prop[4-r-1];
		  }
		} else {
		  // nothing to do here
		}
	      }
            }
        }
    }
    delete [] alpha;
    delete [] theta_prop;
}

void Functions::sample_A_probit (double*** prob,int** seq,int** sequencesMbg, int** pos, Para Para, gsl_rng* p, int* w,int* refpos, double** pos_prob, double** backgmodel_pos, bool ICbg, double** IC, double** beta, int** T_probit, int numofvectors, double*** Y_probit, double** X, int ncol_Y, bool validate ) {
    int M=Para.M;
    int S=Para.S;
    int L=Para.L;
    double pr[L+1];
    for (int m=0; m<M; m++) {
        for (int s=0; s<S; s++) {
            double probit = 0;
            for (int c=0; c<ncol_Y; c++) {
                if (T_probit[m][c] > 0) {
                    probit += beta[m][c]*Y_probit[m][c][s];
                }
            }
            probit = gsl_cdf_ugaussian_P (probit);
            double sum = 0;
            for(int i=0; i<L; i++) {
                pr[i] = probit*pos_prob[m][i];
                double pseq = 1;
                    //if (!validate) {
                    for(int r=0; r<w[m]; r++) {
                        int iposinseq=i-refpos[m]+r;
                        if (iposinseq>=0 && iposinseq<L) {
                            int xi=seq[s][iposinseq];
                            int xi_bg = sequencesMbg[s][iposinseq];
                            double intersec=0;
                            double temp_value=0;
                            for (int k=0; k<M; k++) {
                                if (k!=m) {
                                    if (pos[k][s] < L && pos[k][s]-refpos[k] <= iposinseq && iposinseq < pos[k][s]-refpos[k]+w[k]) {
                                        if (ICbg) {
                                            intersec += IC[k][iposinseq-(pos[k][s]-refpos[k])];
                                            temp_value+=prob[k][iposinseq-(pos[k][s]-refpos[k])][xi]*IC[k][iposinseq-(pos[k][s]-refpos[k])];
                                        } else {
                                            intersec++;
                                            temp_value+=prob[k][iposinseq-(pos[k][s]-refpos[k])][xi];
                                        }
                                    }
                                }
                            }
                            double numerator;
                            if (ICbg) {
                                numerator = (temp_value+prob[m][r][xi]*IC[m][r])/(double)(intersec+IC[m][r]);
                            } else {
                                numerator = (temp_value+prob[m][r][xi])/(double)(intersec+1);
                            }
                            if (intersec > 0) {
                                pseq *= numerator/(temp_value/(double)intersec);
                            } else {
                                pseq *= numerator/backgmodel_pos[iposinseq][xi_bg];
                            }
                        }
                    }
                        //}
                pr[i] *= pseq;
                sum += pr[i];
            }
            pr[L]=1-probit;
            sum += 1-probit;
            int rs = sample(pr, p, sum);
            pos[m][s]=rs;
        }
    }
}

void Functions::sample_B (int** sequences, int** position, int* refpos, double*** prob, int** B, Para Para, gsl_rng* p, int* w, double** IC, bool ICbg) {
    double temp[Para.M];
    for (int s=0; s<Para.S; s++) {
        for (int l=0; l<Para.L; l++) {
            double sum = 0;
            fill_n (temp, Para.M, 0);
            int counterM = 0;
            for (int m=0; m<Para.M; m++) {
                if (position[m][s]-refpos[m]<=l && l<position[m][s]-refpos[m]+w[m] && position[m][s]<Para.L) {
                    counterM++;
                    if (ICbg) {
                        temp[m] = prob[m][l-(position[m][s]-refpos[m])][sequences[s][l]]* IC[m][l-(position[m][s]-refpos[m])];
                    } else {
                        temp[m] = prob[m][l-(position[m][s]-refpos[m])][sequences[s][l]];
                    }
                    sum += temp[m];
                }
            }
            if (counterM > 0) {
                B[s][l] = sample(temp, p, sum);
            } else {
                B[s][l] = Para.M;
            }
        }
    }
}
    //=============================

void Functions::update_refpos (int** D, Para& Para, int* K, gsl_rng* p, int* refpos, int** position, int* w, int** point_detector, double** pos_prob, int** pos_counter) {
    for (int m =0; m<Para.M; m++) {
        double rano = gsl_rng_uniform(p); //if rano<0.5 ==> move one left aka. refpos--
        if ( (rano<0.5 && refpos[m]>0 && pos_counter[m][0]==0)
            || (rano>=0.5 && refpos[m]<w[m]-1 && pos_counter[m][Para.L-1]==0) ) {
            double sum1=0;
            double sum2=0;
            if (rano<0.5) { //move_left
                for (int l=1; l<Para.L; l++) {
                    sum1 += pos_counter[m][l] * log(pos_prob[m][l-1]);
                    sum2 += pos_counter[m][l] * log(pos_prob[m][l]);
                }
            } else { //move_right
                for (int l=0; l<Para.L-1; l++) {
                    sum1 += pos_counter[m][l] * log(pos_prob[m][l+1]);
                    sum2 += pos_counter[m][l] * log(pos_prob[m][l]);
                }
            }
            double choose = gsl_rng_uniform(p);
            double test = sum1-sum2; //acceptance ratio
            if (log(choose) < test) {
                if (rano<0.5) { //move_left
                    refpos[m]--;
                    for (int s=0; s<Para.S ; s++) {
                        if (position[m][s]!=Para.L) {//make sure of motif occurence... we checked before that pos_counter[m][0]==0
                            position[m][s]--;
                        }
                    }
                    for (int l=0; l<Para.L-1; l++) {
                        pos_counter[m][l]=pos_counter[m][l+1];
                    }
                    pos_counter[m][Para.L-1]=0;
                } else { //move_right
                    refpos[m]++;
                    for (int s=0; s<Para.S ; s++) {
                        if (position[m][s]!=Para.L) { //make sure of motif occurence... we checked before that pos_counter[m][Para.L-1]==0
                            position[m][s]++;
                        }
                    }
                    for (int l=Para.L-1; l>0; l--) {
                        pos_counter[m][l]=pos_counter[m][l-1];
                    }
                    pos_counter[m][0]=0;
                }
            }
        }
    }
}

void Functions::update_w (int** sequences, int** position, double*** prob, Para& Para, gsl_rng* p,double** backgmodel_pos, int** sequencesMbg, int* refpos, int* w, bool ICbg, double** IC, double width_geo_prior, int* conv_saver, int wmin_geo_prior, int* center_pal, int** pospair_pal) {
    int M = Para.M;
    int n = Para.S;
    double prior_theta[4]= {Para.z,Para.z,Para.z,Para.z};
    for (int m=0; m<M; m++) {
      double left_right = gsl_rng_uniform(p); //to choose left or right
        if (0 <= left_right && left_right <= 0.5) { //motif length will be changed from right
            double decrease_increase = gsl_rng_uniform(p);//will be used to choose decrease or increase
            if (0 <= decrease_increase && decrease_increase <= 0.5 && refpos[m] < w[m]-1 && w[m] > Para.W0) {
	      //decrease from right
	      if (Para.pal_activated==false || center_pal[m]<0 ||
		  (pospair_pal[m][w[m]-1]<0 && 2*(w[m]-1)-center_pal[m] >= 2*Para.min_halfpal)) {
		//we can remove a column in a palindromic PWM only if this column is not paired
		//and the center of the half palindrom remains >= 2*Para.min_halfpal
               double shiftright[4]={Para.z,Para.z,Para.z,Para.z}; //Para.z
                double expectedfromothers[4]={0,0,0,0};
                double expectedfromothers_tmp[4]={0,0,0,0};
                for (int jj=0; jj<n; jj++) {
                    for (int x=0; x<4; x++) {
                        expectedfromothers_tmp[x]=0;
                    }
                    if (position[m][jj] - refpos[m] + w[m] - 1 < Para.L && position[m][jj] < Para.L) { //check if nec is inside //added_on_12_05
                        int iposinseq= position[m][jj]-refpos[m]+w[m]-1;
                        double intersec=1;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    intersec++;
                                    for (int x=0; x<4; x++) {
                                        expectedfromothers_tmp[x] += prob[k][iposinseq - (position[k][jj]-refpos[k])][x];
                                    }
                                }
                            }
                        }
                        for (int x=0; x<4; x++) {
                            expectedfromothers[x] += expectedfromothers_tmp[x]*(1.0/intersec);
                        }
                        shiftright[sequences[jj][iposinseq]]++;
                    }
                }
                double deltaproposal[4];
                for (int x=0; x<4; x++) {
                    deltaproposal[x]=shiftright[x]-expectedfromothers[x];
                    if (deltaproposal[x]<1) {
                        deltaproposal[x]=1;
                    }
                }
                double logproposaltheta = gsl_ran_dirichlet_lnpdf(4, deltaproposal, prob[m][w[m]-1]);
                double sum1 = 0;
                double sum2 = 0;
                int count_position=0;
                int count_position_intersect=0;
                for (int jj = 0; jj < n; jj++) {//in each sequence
                    if (position[m][jj]-refpos[m]+ w[m]-1 < Para.L && position[m][jj] < Para.L) {//check if the nec is inside the sequence
                        int iposinseq= position[m][jj]-refpos[m]+w[m]-1;
                        int xi=sequences[jj][iposinseq];
                        int xi_bg = sequencesMbg[jj][iposinseq];
                        double intersec=0; //today 12-06-17
                        double temp_value=0;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    if (ICbg) {
                                        intersec += IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi]*IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                    } else {
                                        intersec++;
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi];
                                    }
                                }
                            }
                        }
                        if (ICbg) {
                            sum1 += log((temp_value+prob[m][w[m]-1][xi]*IC[m][w[m]-1])/(double)(intersec+IC[m][w[m]-1]));
                        } else {
                            sum1 += log((temp_value+prob[m][w[m]-1][xi])/(double)(intersec+1)); //before reduction
                        }
                        count_position++;
                        if (intersec > 0) {
                            count_position_intersect++;
                            sum2 += log((temp_value/(double)intersec)); //after reduction
                        } else {
                            sum2 += log(backgmodel_pos[iposinseq][xi_bg]); //after reduction
                        }
                    }
                }
                double Geometric_prior = 0;
                if (w[m] > wmin_geo_prior) {
                    Geometric_prior = ((w[m]-1)-w[m])*log(width_geo_prior);
                }
                double logratiopriorrefpos = log((double)w[m]/(double)(w[m]-1));
                    //double logpriortheta = log(6.0); //corresponds to log of gamma(4)
                
                double logpriortheta = gsl_ran_dirichlet_lnpdf(4, prior_theta, prob[m][w[m]-1]);
		double logratiopriorpal = 0;
		if (Para.pal_activated) {
		  if (center_pal[m]>=0) {
		    //int npossible_center_pal_old = 2*(w[m]-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //int npossible_center_pal_new = 2*((w[m]-1)-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //logratiopriorpal += -log((double)npossible_center_pal_new);
		    //logratiopriorpal -= -log((double)npossible_center_pal_old);
		    logratiopriorpal += log(Para.prior_centerpal[w[m]-1][center_pal[m]]);
		    logratiopriorpal -= log(Para.prior_centerpal[w[m]][center_pal[m]]);
		    
		    if (center_pal[m] >= w[m]) { // otherwise the column wich is removed cannot be paired
		      logratiopriorpal -= log(1-Para.ispaired_inpal_prior);
		    }
		  } else {
		    if (w[m]>=2*Para.min_halfpal && w[m]-1<2*Para.min_halfpal) {
		      logratiopriorpal -= log(1-Para.ispal_prior);
		    }
		  }
		}
                double rano4 = gsl_rng_uniform(p); //to accept or reject the move
                double logpaccept=sum2-sum1+logratiopriorrefpos+logratiopriorpal+logproposaltheta-logpriortheta + Geometric_prior;
                if (log(rano4) < logpaccept) {//then length will be decreased from right
                    w[m]--;
		    //cout << "motif " << m << " decreased from right" << endl;
                }
	      } else {
		//cout << "motif " << m << " attempt of decrease right aborted due to pal constraints" << endl; 
	      }
                    //========================================================
            } else if (0.5 < decrease_increase && decrease_increase <= 1 && w[m] < Para.Wmax) {//increase from right
                double shiftright[4]={Para.z,Para.z,Para.z,Para.z}, theta_right[4]; //Para.z
                double expectedfromothers[4]={0,0,0,0};
                double expectedfromothers_tmp[4]={0,0,0,0};
                for (int jj=0; jj<n; jj++) {
                    for (int x=0; x<4; x++) {
                        expectedfromothers_tmp[x]=0;
                    }
                    if (position[m][jj] - refpos[m] + w[m] < Para.L && position[m][jj] < Para.L) { //check if nec is inside
                        int iposinseq= position[m][jj]-refpos[m]+w[m];
                        double intersec=1;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    intersec++;
                                    for (int x=0; x<4; x++) {
                                        expectedfromothers_tmp[x] += prob[k][iposinseq - (position[k][jj]-refpos[k])][x];
                                    }
                                }
                            }
                        }
                        for (int x=0; x<4; x++) {
                            expectedfromothers[x] += expectedfromothers_tmp[x]*(1.0/intersec);
                        }
                        shiftright[sequences[jj][iposinseq]]++;
                    }
                }
                double deltaproposal[4];
                for (int x=0; x<4; x++) {
                    deltaproposal[x]=shiftright[x]-expectedfromothers[x];
                    if (deltaproposal[x]<1) {
                        deltaproposal[x]=1;
                    }
                }
                gsl_ran_dirichlet(p, 4, deltaproposal, theta_right);//compute_theta_right
                double logproposaltheta = gsl_ran_dirichlet_lnpdf(4, deltaproposal, theta_right);
                double IC_right = computeIC(theta_right);
                double sum1 = 0;
                double sum2 = 0;
                int count_position=0;
                int count_position_intersect=0;
                for(int jj = 0; jj < n; jj++) {//in each sequence
                    if (position[m][jj]-refpos[m]+w[m] < Para.L && position[m][jj] < Para.L) {//check if the nec is inside the sequence
                        int iposinseq = position[m][jj]-refpos[m]+w[m];
                        int xi = sequences[jj][iposinseq];
                        int xi_bg = sequencesMbg[jj][iposinseq];
                        double intersec = 0;
                        double temp_value = 0;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    if (ICbg) {
                                        intersec += IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi]*IC[k][iposinseq - (position[k][jj]-refpos[k])];//continue from here!

                                    } else {
                                        intersec++;
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi];
                                    }
                                }
                            }
                        }
                        count_position++;
                        if (intersec > 0) {
                            count_position_intersect++;
                            sum1 += log((temp_value)/(double)(intersec)); //before
                            if (ICbg) {
                                sum2 += log((temp_value + theta_right[xi]*IC_right)/(double)(intersec+IC_right));
                            } else {
                                sum2 += log((temp_value + theta_right[xi])/(double)(intersec+1)); //after
                            }
                        } else {
                            sum1 += log(backgmodel_pos[iposinseq][xi_bg]); //before
                            sum2 += log(theta_right[xi]); //after
                        }
                    }
                }
                double Geometric_prior = 0;
                if (w[m] >= wmin_geo_prior) {
                    Geometric_prior = ((w[m]+1)-w[m])*log(width_geo_prior);
                }
                double logratiopriorrefpos = log((double)w[m]/(double)(w[m]+1));
                    //double logpriortheta = log(6.0); //corresponds to gamma (4)
                double logpriortheta = gsl_ran_dirichlet_lnpdf(4, prior_theta, theta_right);
		double logratiopriorpal = 0;
		if (Para.pal_activated) {
		  if (center_pal[m]>=0) {
		    //int npossible_center_pal_old = 2*(w[m]-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //int npossible_center_pal_new = 2*((w[m]+1)-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //logratiopriorpal += -log((double)npossible_center_pal_new);
		    //logratiopriorpal -= -log((double)npossible_center_pal_old);
		    logratiopriorpal += log(Para.prior_centerpal[w[m]+1][center_pal[m]]);
		    logratiopriorpal -= log(Para.prior_centerpal[w[m]][center_pal[m]]);
		    if (center_pal[m] >= w[m]+1) { // otherwise the column wich is added cannot be paired
		      logratiopriorpal += log(1-Para.ispaired_inpal_prior);
		    }
		  } else {
		    if (w[m]<2*Para.min_halfpal && w[m]+1>=2*Para.min_halfpal) {
		      logratiopriorpal += log(1-Para.ispal_prior);
		    }
		  }
		}
		
		double rano4 = gsl_rng_uniform(p); //to accept or reject the move
                double logpaccept=sum2-sum1+logratiopriorrefpos+logpriortheta+logratiopriorpal-logproposaltheta + Geometric_prior;

                if (log(rano4) < logpaccept) {//then length will be increased from right
 		  //cout << "motif " << m << " increased from right" << endl;
                   IC[m][w[m]] = IC_right;
                    w[m]++;
		    if (Para.pal_activated && center_pal[m]>0) {
		      pospair_pal[m][w[m]-1]=-1; // the new column is unpaired
		    }
                    for (int r=0; r<4; r++) {
                        prob[m][w[m]-1][r]=theta_right[r];
                    }
                }
            }
                //================================================================
        } else if (0.5 < left_right && left_right <= 1) { //motif length will be changed from left
            double decrease_increase = gsl_rng_uniform(p);//will be used to choose decrease or increase
            if (0 <= decrease_increase && decrease_increase <= 0.5 && refpos[m] > 0 && w[m] > Para.W0) {//decrease from left
               
	      if (Para.pal_activated==false || center_pal[m]<0 ||
		  (pospair_pal[m][0]<0 && center_pal[m]-2 >= 2*Para.min_halfpal)) {
		//we can remove a column in a palindromic PWM only if this column is not paired
		//and the center of the half palindrom remains >= 2*Para.min_halfpal

                double shiftleft[4]={Para.z,Para.z,Para.z,Para.z}; //Para.z
                double expectedfromothers[4]={0,0,0,0};
                double expectedfromothers_tmp[4]={0,0,0,0};
                
                for (int jj=0; jj<n; jj++) {
                    for (int x=0; x<4; x++) {
                        expectedfromothers_tmp[x]=0;
                    }
                    if (position[m][jj]-refpos[m] >= 0 && position[m][jj] < Para.L) { //check if nec is inside
                        int iposinseq= position[m][jj]-refpos[m];
                        double intersec=1;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    intersec++;
                                    for (int x=0; x<4; x++) {
                                        expectedfromothers_tmp[x] += prob[k][iposinseq - (position[k][jj]-refpos[k])][x];
                                    }
                                }
                            }
                        }
                        for (int x=0; x<4; x++) {
                            expectedfromothers[x] += expectedfromothers_tmp[x]*(1.0/intersec);
                        }
                        shiftleft[sequences[jj][iposinseq]]++;
                    }
                }
                double deltaproposal[4];
                for (int x=0; x<4; x++) {
                    deltaproposal[x]=shiftleft[x]-expectedfromothers[x];
                    if (deltaproposal[x]<1) {
                        deltaproposal[x]=1;
                    }
                }
                double logproposaltheta = gsl_ran_dirichlet_lnpdf(4, deltaproposal, prob[m][0]);
                
                double sum1 = 0;
                double sum2 = 0;
                int count_position=0;
                int count_position_intersect=0;
                    //===========likelihood=====================
                for(int jj = 0; jj < n; jj++) {//in each sequence
                    if (position[m][jj]-refpos[m] >= 0 && position[m][jj] < Para.L) {//check if the nec is in the seq
                        int iposinseq= position[m][jj]-refpos[m];
                        int xi=sequences[jj][iposinseq];
                        int xi_bg = sequencesMbg[jj][iposinseq];
                        double intersec=0;
                        double temp_value=0;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    if (ICbg) {
                                        intersec += IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi]*IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                    } else {
                                        intersec++;
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi];
                                    }
                                }
                            }
                        }
                        if (ICbg) {
                            sum1 += log((temp_value+prob[m][0][xi]*IC[m][0])/(double)(intersec+IC[m][0]));
                        } else {
                            sum1 += log((temp_value+prob[m][0][xi])/(double)(intersec+1)); //before reduction
                        }
                        count_position++;
                        if (intersec > 0) {
                            count_position_intersect++;
                            sum2 += log((temp_value/(double)intersec)); //after reduction
                        } else {//there's no intersection
                            sum2 += log(backgmodel_pos[iposinseq][xi_bg]); //after reduction
                        }
                    }
                }
                double Geometric_prior = 0;
                if (w[m] > wmin_geo_prior) {
                    Geometric_prior = ((w[m]-1)-w[m])*log(width_geo_prior);
                }
                double logratiopriorrefpos = log((double)w[m]/(double)(w[m]-1));
                    //double logpriortheta = log(6.0); //corresponds to gamma (4)
                double logpriortheta = gsl_ran_dirichlet_lnpdf(4, prior_theta, prob[m][0]);
		double logratiopriorpal = 0;
		if (Para.pal_activated) {
		  if (center_pal[m]>=0) {
		    //int npossible_center_pal_old = 2*(w[m]-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //int npossible_center_pal_new = 2*((w[m]-1)-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //logratiopriorpal += -log((double)npossible_center_pal_new);
		    //logratiopriorpal -= -log((double)npossible_center_pal_old);
		    logratiopriorpal += log(Para.prior_centerpal[w[m]-1][center_pal[m]-2]);
		    logratiopriorpal -= log(Para.prior_centerpal[w[m]][center_pal[m]]);
		    if (center_pal[m] <= w[m]) { // otherwise the column wich is removed cannot be paired
		      logratiopriorpal -= log(1-Para.ispaired_inpal_prior);
		    }
		  } else {
		    if (w[m]>=2*Para.min_halfpal && w[m]-1<2*Para.min_halfpal) {
		      logratiopriorpal -= log(1-Para.ispal_prior);
		    }
		  }
		}
 
		
                double rano4 = gsl_rng_uniform(p); //to accept or reject the move
                double logpaccept=sum2-sum1+logratiopriorrefpos+logproposaltheta+logratiopriorpal-logpriortheta + Geometric_prior;

                if (log(rano4) < logpaccept) { //then length will be decreased from left
		  //cout << "motif " << m << " decreased from left" << endl;
                    conv_saver[m]++;
                    refpos[m]--;
                    w[m]--;
		    if (Para.pal_activated && center_pal[m]>0) {
		      center_pal[m]-=2;
		      for (int ww=0; ww<w[m]; ww++) {
			if (pospair_pal[m][ww+1]>=0) {
			  pospair_pal[m][ww]=pospair_pal[m][ww+1]-1;
			} else {
			  pospair_pal[m][ww]= -1;
			}
		      }
		      pospair_pal[m][w[m]]=-1;
		    }
                    for (int ww=0; ww<w[m]; ww++) {
		      IC[m][ww]=IC[m][ww+1];
                        for (int r=0; r<4; r++) {
                            prob[m][ww][r]=prob[m][ww+1][r];
                        }
                    }
		}
	      } else {
		//cout << "motif " << m << " attempt of decrease left aborted due to pal constraints" << endl;
	      }//====================================================
            } else if (0.5 < decrease_increase && decrease_increase <= 1 && w[m] < Para.Wmax) {//increase from left //refpos[m] < w[m]-1!
                double shiftleft[4]={Para.z,Para.z,Para.z,Para.z}, theta_left[4]; //Para.z
                double expectedfromothers[4]={0,0,0,0};
                double expectedfromothers_tmp[4]={0,0,0,0};
                for (int jj=0; jj<n; jj++) {
                    for (int x=0; x<4; x++) {
                        expectedfromothers_tmp[x]=0;
                    }
                    if (position[m][jj]-refpos[m]-1 >= 0 && position[m][jj] < Para.L) { //check if nec is inside
                        int iposinseq = position[m][jj]-refpos[m]-1;
                        double intersec=1;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    intersec++;
                                    for (int x=0; x<4; x++) {
                                        expectedfromothers_tmp[x] += prob[k][iposinseq - (position[k][jj]-refpos[k])][x];
                                    }
                                }
                            }
                        }
                        for (int x=0; x<4; x++) {
                            expectedfromothers[x] += expectedfromothers_tmp[x]*(1.0/intersec);
                        }
                        shiftleft[sequences[jj][iposinseq]]++;
                    }
                }
                double deltaproposal[4];
                for (int x=0; x<4; x++) {
                    deltaproposal[x]=shiftleft[x]-expectedfromothers[x];
                    if (deltaproposal[x]<1) {
                        deltaproposal[x]=1;
                    }
                }
                gsl_ran_dirichlet(p, 4, deltaproposal, theta_left);
                double logproposaltheta = gsl_ran_dirichlet_lnpdf(4, deltaproposal, theta_left);
                double IC_left = computeIC(theta_left);
                double sum1 = 0;
                double sum2 = 0;
                int count_position=0;
                int count_position_intersect=0;
                for(int jj = 0; jj < n; jj++) {//in each sequence
                    if (position[m][jj]-refpos[m]-1 >= 0 && position[m][jj] < Para.L) {//check if the nec is in the seq
                        int iposinseq = position[m][jj]-refpos[m]-1;
                        int xi = sequences[jj][iposinseq];
                        int xi_bg = sequencesMbg[jj][iposinseq];
                        double intersec = 0; //today
                        double temp_value = 0;
                        for (int k=0; k<M; k++) {
                            if (k!=m) {
                                if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                    if (ICbg) {
                                        intersec += IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi]*IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                    } else {
                                        intersec++;
                                        temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi];
                                    }
                                }
                            }
                        }
                        count_position++;
                        if (intersec > 0) { //there's intersection
                            count_position_intersect++;
                            sum1 += log((temp_value)/(double)(intersec)); //before
                            if (ICbg) {
                                sum2 += log((temp_value + theta_left[xi]*IC_left)/(double)(intersec+IC_left));
                            } else {
                                sum2 += log((temp_value + theta_left[xi])/(double)(intersec+1)); //after
                            }
                        } else { //there's no intersection
                            sum1 += log(backgmodel_pos[iposinseq][xi_bg]); //before
                            sum2 += log(theta_left[xi]); //after
                        }
                    }
                }
                double Geometric_prior = 0;
                if (w[m] >= wmin_geo_prior) {
                    Geometric_prior = ((w[m]+1)-w[m])*log(width_geo_prior);
                }
                double logratiopriorrefpos = log((double)w[m]/(double)(w[m]+1));
                    //double logpriortheta = log(6.0); //corresponds to gamma (4)
                double logpriortheta = gsl_ran_dirichlet_lnpdf(4, prior_theta, theta_left);
		double logratiopriorpal = 0;
		if (Para.pal_activated) {
		  if (center_pal[m]>=0) {
		    //int npossible_center_pal_old = 2*(w[m]-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //int npossible_center_pal_new = 2*((w[m]+1)-Para.min_halfpal)-2*Para.min_halfpal+1;
		    //logratiopriorpal += -log((double)npossible_center_pal_new);
		    //logratiopriorpal -= -log((double)npossible_center_pal_old);
		    logratiopriorpal += log(Para.prior_centerpal[w[m]+1][center_pal[m]+2]);
		    logratiopriorpal -= log(Para.prior_centerpal[w[m]][center_pal[m]]);

		    if (center_pal[m]+2 <= w[m]+1) { // otherwise the column wich is added cannot be paired
		      logratiopriorpal += log(1-Para.ispaired_inpal_prior);
		    }
		  } else {
		    if (w[m]<2*Para.min_halfpal && w[m]+1>=2*Para.min_halfpal) {
		      logratiopriorpal += log(1-Para.ispal_prior);
		    }
		  }
		}
                
                double rano4 = gsl_rng_uniform(p); //to accept or reject the move
                double logpaccept=sum2-sum1+logratiopriorrefpos+logpriortheta+logratiopriorpal-logproposaltheta + Geometric_prior;
                
                if (log(rano4) < logpaccept) {//then length will be increased from left
 		  //cout << "motif " << m << " increased from left" << endl;
                    conv_saver[m]--;
                    refpos[m]++;
                    w[m]++;
		    if (Para.pal_activated && center_pal[m]>0) {
		      center_pal[m]+=2;
		      for (int ww=w[m]-1; ww>=1; ww--) {
			if (pospair_pal[m][ww-1]>=0) {
			  pospair_pal[m][ww]=pospair_pal[m][ww-1]+1;
			} else {
			  pospair_pal[m][ww]=-1;
			}
		      }
		      pospair_pal[m][0]=-1; // the new position is unpaired
 		    }
                    for (int ww=w[m]-1; ww>=1; ww--) { // PN 2018/06/11 corrected a bug here "ww>1" -> "w>=1"
                        IC[m][ww]=IC[m][ww-1];
                        for (int r=0; r<4; r++) {
                            prob[m][ww][r]=prob[m][ww-1][r];
                        }
                    }
                   IC[m][0]=IC_left;
                    for (int r=0; r<4; r++) {
                        prob[m][0][r]=theta_left[r];
                    }
                }
            }
        }
    }
}


void Functions::Update_BackgroundMr_position (int** sequencesMbg, int** B, Para& Para, int rbackg, gsl_rng* p, double** backgmodel_pos, bool positionwisebg) { //position_dependent or no! Both are here
    int codemax = (int)((pow(4,rbackg+2)-4.0)/3.0);
    double counts[codemax];
    if (positionwisebg) { //position_dependent
        for (int i=0; i<Para.L; i++) {
            fill_n (counts, codemax, Para.z0); //Para.z //background!!!
            for (int j=0; j<Para.S; j++) {
                if (B[j][i]==Para.M) {
                    counts[sequencesMbg[j][i]]++; //prior is  stronger now!!
                }
            }
            int pointcount = 0;
            int pointbackgmodel = 0;
            int INC = codemax/4;
            for (int inc = 0; inc<INC ; inc++) {
                double tempcount[4];
                double tempbackgmodel[4];
                for (int r=0; r<4; r++) {
                    tempcount[r]=counts[pointcount];
                    pointcount++;
                }
                gsl_ran_dirichlet (p, 4, tempcount, tempbackgmodel);
                for (int r=0; r<4; r++) {
                    backgmodel_pos[i][pointbackgmodel]=tempbackgmodel[r];
                    pointbackgmodel++;
                }
            }
        }
    } else { //position_independent
        fill_n (counts, codemax, Para.z0); //Para.z //background!!!
        for (int i=0; i<Para.L; i++) {
            for (int j=0; j<Para.S; j++) {
                if (B[j][i]==Para.M) {
                    counts[sequencesMbg[j][i]]++; //prior is  stronger now!!
                }
            }
        }
        int pointcount = 0;
        int pointbackgmodel = 0;
        int INC = codemax/4;
        for (int inc = 0; inc<INC ; inc++) {
            double tempcount[4];
            double tempbackgmodel[4];
            for (int r=0; r<4; r++) {
                tempcount[r]=counts[pointcount];
                pointcount++;
            }
            gsl_ran_dirichlet (p, 4, tempcount, tempbackgmodel);
            for (int r=0; r<4; r++) {
                backgmodel_pos[0][pointbackgmodel]=tempbackgmodel[r];
                pointbackgmodel++;
            }
        }
        for (int i=1; i<Para.L; i++) {
            for (int w=0; w<codemax; w++) {
                backgmodel_pos[i][w]=backgmodel_pos[0][w];
            }
        }
    }
}
    //===

void Functions::Update_Background_order_nopos (gsl_rng* p, int &rbackg, int rbackmax,  Para& Para, int** B, int** &sequencesMbg, Sequences* seqs) {
    int rbackg_temp = rbackg;
    int codemax = (int)((pow(4,rbackg+2)-4.0)/3.0);
    int codemax_temp;
    double random = gsl_rng_uniform(p); // to increase/decrease the order
    if ( (0 <= random && random <= 0.5 && rbackg < rbackmax) || (0.5 < random && random <= 1 && rbackg > 0) ) {
        if (0 <= random && random <= 0.5 && rbackg < rbackmax) { //increase
            rbackg_temp = rbackg + 1;
        } else if (0.5 < random && random <= 1 && rbackg > 0) { //decrease
            rbackg_temp = rbackg - 1;
        }
        codemax_temp = (int)((pow(4,rbackg_temp+2)-4.0)/3.0);
        int** sequencesMbg_temp = seqs->sequencesMr(rbackg_temp);
        double counts[codemax];
        fill_n (counts, codemax, Para.z0); //Para.z //background!!!
        for (int j=0; j<Para.S; j++) {
            for (int i=0; i<Para.L; i++) {
                if (B[j][i]==Para.M) {
                    counts[sequencesMbg[j][i]]++;
                }
            }
        }
        double counts_temp[codemax_temp];
        fill_n (counts_temp, codemax_temp, Para.z0); //Para.z //background!!!
        for (int j=0; j<Para.S; j++) {
            for (int i=0; i<Para.L; i++) {
                if (B[j][i]==Para.M) {
                    counts_temp[sequencesMbg_temp[j][i]]++;
                }
            }
        }
        double multiofgamma = 0;
        double multiofprior = 0;
        for (int c=0; c<codemax ; c++) {//current
            multiofgamma += gsl_sf_lngamma (counts[c]);
            multiofprior += gsl_sf_lngamma (Para.z0); //Para.z //background!!!
        }
        int pointcount=0;
        int INC = codemax/4;
        double gammaofsum = 0;
        double gammaofprior = 0;
        for (int inc = 0; inc<INC ; inc++) {
            double sum = 0 ;
            double sumprior = 0;
            for (int r=0; r<4; r++) {
                sum += counts[pointcount];
                sumprior += Para.z0; //para.z0
                pointcount++;
            }
            gammaofsum += gsl_sf_lngamma (sum);
            gammaofprior += gsl_sf_lngamma (sumprior);
        }
        double multiofgamma_temp = 0;
        double multiofprior_temp = 0;
        for (int c=0; c<codemax_temp ; c++) {//proposed
            multiofgamma_temp += gsl_sf_lngamma (counts_temp[c]);
            multiofprior_temp += gsl_sf_lngamma (Para.z0); //Para.z //background!!!
        }
        int pointcount_temp=0;
        int INC_temp = codemax_temp/4;
        double gammaofsum_temp = 0;
        double gammaofprior_temp = 0;
        for (int inc = 0; inc<INC_temp ; inc++) {
            double sum_temp = 0 ;
            double sumprior_temp = 0;
            for (int r=0; r<4; r++) {
                sum_temp += counts_temp[pointcount_temp];
                sumprior_temp += Para.z0; //Para.z //background!!!
                pointcount_temp++;
            }
            gammaofsum_temp += gsl_sf_lngamma (sum_temp);
            gammaofprior_temp += gsl_sf_lngamma (sumprior_temp);
        }
        double liklihood = multiofgamma - multiofprior + gammaofprior - gammaofsum;
        double liklihood_temp = multiofgamma_temp - multiofprior_temp + gammaofprior_temp - gammaofsum_temp;
        double logpaccept = liklihood_temp - liklihood;
        double randomagain = gsl_rng_uniform(p);
        if (log(randomagain) < logpaccept) {
            rbackg = rbackg_temp;
            sequencesMbg = sequencesMbg_temp;
        }
    }
}
    //===========22_05_2017=======
void Functions::Update_Background_order_pos (gsl_rng* p, int &rbackg, int rbackmax,  Para& Para, int** B, int** &sequencesMbg, Sequences* seqs) {
    int rbackg_temp = rbackg;
    int codemax = (int)((pow(4,rbackg+2)-4.0)/3.0);
    int codemax_temp;
    double random = gsl_rng_uniform(p); // to increase/decrease the order
    if ( (0 <= random && random <= 0.5 && rbackg < rbackmax) || (0.5 < random && random <= 1 && rbackg > 0) ) {
        if (0 <= random && random <= 0.5 && rbackg < rbackmax) { //increase
            rbackg_temp = rbackg + 1;
        } else if (0.5 < random && random <= 1 && rbackg > 0) { //decrease
            rbackg_temp = rbackg - 1;
        }
        codemax_temp = (int)((pow(4,rbackg_temp+2)-4.0)/3.0);
        int** sequencesMbg_temp = seqs->sequencesMr(rbackg_temp);
        double** counts = new double* [Para.L];
        for (int l=0; l<Para.L; l++) {
            counts[l] = new double [codemax];
            fill_n (counts[l], codemax, Para.z0); //Para.z //background!!!
        }//recent
        for (int j=0; j<Para.S; j++) {
            for (int i=0; i<Para.L; i++) {
                if (B[j][i]==Para.M) {
                    counts[i][sequencesMbg[j][i]]++;//recent
                }
            }
        }
        double** counts_temp = new double* [Para.L];
        for (int l=0; l<Para.L; l++) {
            counts_temp[l] = new double [codemax_temp];
            fill_n (counts_temp[l], codemax_temp, Para.z0); //Para.z //background!!!
        }//recent
        for (int j=0; j<Para.S; j++) {
            for (int i=0; i<Para.L; i++) {
                if (B[j][i]==Para.M) {
                    counts_temp[i][sequencesMbg_temp[j][i]]++;//recent
                }
            }
        }
        double multiofgamma = 0;
        double multiofprior = 0;
        double gammaofsum = 0;
        double gammaofprior = 0;
        int INC = codemax/4;
        for (int l=0; l<Para.L; l++) {//recent
            int pointcount=0;
            for (int c=0; c<codemax ; c++) {
                multiofgamma += gsl_sf_lngamma (counts[l][c]);
                multiofprior += gsl_sf_lngamma (Para.z0); //Para.z //background!!!
            }
            for (int inc = 0; inc<INC ; inc++) {
                double sum = 0 ;
                double sumprior = 0;
                for (int r=0; r<4; r++) {
                    sum += counts[l][pointcount];
                    sumprior += Para.z0; //Para.z //background!!!
                    pointcount++;
                }
                gammaofsum += gsl_sf_lngamma (sum);
                gammaofprior += gsl_sf_lngamma (sumprior);
            }
        }
        double multiofgamma_temp = 0;
        double multiofprior_temp = 0;
        double gammaofsum_temp = 0;
        double gammaofprior_temp = 0;
        int INC_temp = codemax_temp/4;
        for (int l=0; l<Para.L; l++) {
            int pointcount_temp=0;
            for (int c=0; c<codemax_temp ; c++) {
                multiofgamma_temp += gsl_sf_lngamma (counts_temp[l][c]);
                multiofprior_temp += gsl_sf_lngamma (Para.z0); //Para.z //background!!!
            }
            for (int inc = 0; inc<INC_temp ; inc++) {
                double sum_temp = 0 ;
                double sumprior_temp = 0;
                for (int r=0; r<4; r++) {
                    sum_temp += counts_temp[l][pointcount_temp];
                    sumprior_temp += Para.z0; //Para.z //background!!!
                    pointcount_temp++;
                }
                gammaofsum_temp += gsl_sf_lngamma (sum_temp);
                gammaofprior_temp += gsl_sf_lngamma (sumprior_temp);
            }
        }
        double liklihood = multiofgamma - multiofprior + gammaofprior - gammaofsum;
        double liklihood_temp = multiofgamma_temp - multiofprior_temp + gammaofprior_temp - gammaofsum_temp;
        double logpaccept = liklihood_temp - liklihood;
        double randomagain = gsl_rng_uniform(p);
        if (log(randomagain) < logpaccept) {
            rbackg = rbackg_temp;
            sequencesMbg = sequencesMbg_temp;
        }
        for (int l=0; l<Para.L; l++) {
            delete [] counts [l];
            delete [] counts_temp[l];
        }
        delete [] counts;
        delete [] counts_temp;
    }
}
    //================end_of_22_05_2017==================
void Functions::Update_Background_order (gsl_rng* p, int &rbackg, int rbackmax,  Para& Para, int** B, int** &sequencesMbg, Sequences* seqs, bool positionwisebg)
{
    if (positionwisebg) {
        Update_Background_order_pos (p, rbackg, rbackmax, Para, B, sequencesMbg, seqs);
    } else {
        Update_Background_order_nopos (p, rbackg, rbackmax, Para, B, sequencesMbg, seqs);
    }
}
    //================Start_of_24_05_2017================

void Functions::Shift_motif (gsl_rng* p, Para Para, int* w, double** pos_prob, int** position, int* refpos, int** sequences, int** sequencesMbg, double*** prob, double** backgmodel_pos, bool ICbg, double** IC, int* conv_saver, int* center_pal, int** pospair_pal) {
    double prior_theta[4]= {Para.z,Para.z,Para.z,Para.z};
    for (int m = 0; m<Para.M; m++) {
        double left_right = gsl_rng_uniform(p);
	bool ok_left_right = true;
	if (0 <= left_right && left_right <= 0.5) { // shift right (remove left, add right)
	  if (refpos[m] == 0) {
	    ok_left_right = false;
	  }
	  if (Para.pal_activated && center_pal[m]>0) {
	    if (center_pal[m]-2 < 2*Para.min_halfpal) { // new center_pal needs to comply with Para.min_halfpal
	      ok_left_right = false;
	    }
	    if (pospair_pal[m][0]>=0) { // cannot remove a column that is paired
	      ok_left_right = false;
	    }
	  }
	} else if (0.5 < left_right && left_right <= 1) { // shift left (remove right, add left)
	  if (refpos[m] == w[m]-1) {
	    ok_left_right = false;	    
	  }
	  if (Para.pal_activated && center_pal[m]>0) {
	    if (2*w[m]-(center_pal[m]+2) < 2*Para.min_halfpal) {
	      ok_left_right = false;
	    }
	    if (pospair_pal[m][w[m]-1]>=0) { // cannot remove a column that is paired
	      ok_left_right = false;
	    }	    
	  }
	}
	
        if (ok_left_right) {
            int new_shift=0, old_shift=0, pos_new=0, pos_old=0;
            if (0 <= left_right && left_right <= 0.5) { //motif will be shifted right //new=right, old=left
                new_shift = -refpos[m]+w[m];
                old_shift = -refpos[m];
                pos_new = w[m]-1;
                pos_old = 0;
            } else if (0.5 < left_right && left_right <= 1) { //left
                new_shift = -refpos[m]-1;
                old_shift = -refpos[m]+w[m]-1;
                pos_new = 0;
                pos_old = w[m]-1;
            }
            double shift_new[4]={Para.z,Para.z,Para.z,Para.z}, theta_new[4]; //Para.z
            double shift_old[4]={Para.z,Para.z,Para.z,Para.z}; //Para.z
            double expectedfromothers_new[4]={0,0,0,0}, expectedfromothers_old[4]={0,0,0,0};
            double expectedfromothers_tmp_new[4]={0,0,0,0}, expectedfromothers_tmp_old[4]={0,0,0,0};
            for (int jj=0; jj<Para.S; jj++) {
                for (int x=0; x<4; x++) {
                    expectedfromothers_tmp_new[x]=0;
                    expectedfromothers_tmp_old[x]=0;
                }
                int iposinseq= position[m][jj] + new_shift;
                if (iposinseq < Para.L && iposinseq >= 0 && position[m][jj] < Para.L) { //check if nec is inside
                    double intersec=1;
                    for (int k=0; k<Para.M; k++) {
                        if (k!=m) {
                            if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                intersec++;
                                for (int x=0; x<4; x++) {
                                    expectedfromothers_tmp_new[x] += prob[k][iposinseq - (position[k][jj]-refpos[k])][x];
                                }
                            }
                        }
                    }
                    for (int x=0; x<4; x++) {
                        expectedfromothers_new[x] += expectedfromothers_tmp_new[x]*(1.0/intersec);
                    }
                    shift_new[sequences[jj][iposinseq]]++;
                }//first_if_ends_here
                iposinseq = position[m][jj] + old_shift;
                if (iposinseq < Para.L && iposinseq >= 0 && position[m][jj] < Para.L) { //check if nec is inside
                    double intersec=1;
                    for (int k=0; k<Para.M; k++) {
                        if (k!=m) {
                            if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                intersec++;
                                for (int x=0; x<4; x++) {
                                    expectedfromothers_tmp_old[x] += prob[k][iposinseq - (position[k][jj]-refpos[k])][x];
                                }
                            }
                        }
                    }
                    for (int x=0; x<4; x++) {
                        expectedfromothers_old[x] += expectedfromothers_tmp_old[x]*(1.0/intersec);
                    }
                    shift_old[sequences[jj][iposinseq]]++;
                }
            }//the_for_loop_ends_here
            double deltaproposal_new[4];
            double deltaproposal_old[4];
            for (int x=0; x<4; x++) { //calculate_deltaproposal
                deltaproposal_new[x]=shift_new[x]-expectedfromothers_new[x];
                deltaproposal_old[x]=shift_old[x]-expectedfromothers_old[x];
                if (deltaproposal_new[x]<1) {
                    deltaproposal_new[x]=1;
                }
                if (deltaproposal_old[x]<1) {
                    deltaproposal_old[x]=1;
                }
            }
            gsl_ran_dirichlet(p, 4, deltaproposal_new, theta_new);//compute_theta_right
            double logproposaltheta_new = gsl_ran_dirichlet_lnpdf(4, deltaproposal_new, theta_new);
            double logproposaltheta_old = gsl_ran_dirichlet_lnpdf(4, deltaproposal_old, prob[m][pos_old]);
            double logpriortheta_new = gsl_ran_dirichlet_lnpdf(4, prior_theta, theta_new);
            double logpriortheta_old = gsl_ran_dirichlet_lnpdf(4, prior_theta, prob[m][pos_old]);
            
            double sum1_new = 0, sum1_old = 0;
            double sum2_new = 0, sum2_old = 0;
            for(int jj = 0; jj < Para.S; jj++) {//in each sequence
                int iposinseq= position[m][jj] + new_shift;
                if (iposinseq < Para.L && iposinseq >= 0 && position[m][jj] < Para.L) {//check if the nec is inside the sequence
                    int xi = sequences[jj][iposinseq];
                    int xi_bg = sequencesMbg[jj][iposinseq];
                    double intersec = 0; //liklihood
                    double temp_value = 0;
                    for (int k=0; k<Para.M; k++) {
                        if (k!=m) {
                            if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                if (ICbg) {
                                    intersec += IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                    temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi]*IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                } else {
                                    intersec++;
                                    temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi];
                                }
                            }
                        }
                    }
                    if (intersec > 0) {
                            sum1_new += log((temp_value)/(double)(intersec)); //current
                        if (ICbg) {
                            sum2_new += log((temp_value + theta_new[xi]*computeIC(theta_new))/(double)(intersec+computeIC(theta_new)));
                        } else {
                            sum2_new += log((temp_value + theta_new[xi])/(double)(intersec+1)); //proposed
                        }
                    } else {
                        sum1_new += log(backgmodel_pos[iposinseq][xi_bg]); //current
                        sum2_new += log(theta_new[xi]); //proposed
                    }
                }//first_if_ends_here
                iposinseq = position[m][jj] + old_shift;
                if (iposinseq < Para.L && iposinseq >= 0 && position[m][jj] < Para.L) {//check if the nec is in the seq
                    int xi=sequences[jj][iposinseq];
                    int xi_bg = sequencesMbg[jj][iposinseq];
                    double intersec=0;
                    double temp_value=0;
                    for (int k=0; k<Para.M; k++) {
                        if (k!=m) {
                            if (position[k][jj] < Para.L && position[k][jj]-refpos[k] <=  iposinseq && iposinseq < position[k][jj]-refpos[k]+w[k]) {
                                if (ICbg) {
                                    intersec += IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                    temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi]*IC[k][iposinseq - (position[k][jj]-refpos[k])];
                                } else {
                                    intersec++;
                                    temp_value += prob[k][iposinseq - (position[k][jj]-refpos[k])][xi];
                                }
                            }
                        }
                    }
                    if (ICbg) {
                        sum1_old += log((temp_value+prob[m][pos_old][xi]*IC[m][pos_old])/(double)(intersec+IC[m][pos_old]));
                    } else {
                    sum1_old += log((temp_value+prob[m][pos_old][xi])/(double)(intersec+1)); //current
                    }
                    if (intersec > 0) { //there's intersection
                        sum2_old += log((temp_value/(double)intersec)); //proposed
                    } else {
                        sum2_old += log(backgmodel_pos[iposinseq][xi_bg]); //proposed
                    }
                }
            }
	    double logratiopriorpal = 0;
	    if (Para.pal_activated && center_pal[m]>0) {
	      if (0.5 < left_right && left_right <= 1) { // shift left (remove right, add left)
		if (center_pal[m] >= w[m]) {
		  logratiopriorpal -= log(1-Para.ispaired_inpal_prior);
		}
		if (center_pal[m]+2 <= w[m]) {
		  logratiopriorpal += log(1-Para.ispaired_inpal_prior);
		}
		logratiopriorpal += log(Para.prior_centerpal[w[m]][center_pal[m]+2]);
		logratiopriorpal -= log(Para.prior_centerpal[w[m]][center_pal[m]]);
	      } else { // shift right (remove left, add right)
		if (center_pal[m] <= w[m]) {
		  logratiopriorpal -= log(1-Para.ispaired_inpal_prior);
		}
		if (center_pal[m]-2 >= w[m]) {
		  logratiopriorpal += log(1-Para.ispaired_inpal_prior);
		}
		logratiopriorpal += log(Para.prior_centerpal[w[m]][center_pal[m]-2]);
		logratiopriorpal -= log(Para.prior_centerpal[w[m]][center_pal[m]]);
	      }
	    }

	    double rano4 = gsl_rng_uniform(p); //to accept or reject the move
            double logpaccept=sum2_new-sum1_new-logproposaltheta_new+sum2_old-sum1_old+logproposaltheta_old+logratiopriorpal+logpriortheta_new-logpriortheta_old;
            
            if (log(rano4) < logpaccept) {
                if (0 <= left_right && left_right <= 0.5) {//shift_right=increase_right+decrease_left
		  //cout << "motif " << m << " shifted to right" << endl;
                    conv_saver[m]++;
                    refpos[m]--;
                    for (int ww=0; ww<w[m]-1; ww++) {
                        IC[m][ww]=IC[m][ww+1];
                        for (int r=0; r<4; r++) {
                            prob[m][ww][r]=prob[m][ww+1][r];
                        }
                    }
                    IC[m][pos_new]=computeIC(theta_new);
                    for (int r=0; r<4; r++) {
                        prob[m][pos_new][r]=theta_new[r];
                    }
		    if (Para.pal_activated && center_pal[m]>0) {
		      center_pal[m]-=2;
		      for (int ww=0; ww<w[m]-1; ww++) {
			if (pospair_pal[m][ww+1]>=0) {
			  pospair_pal[m][ww]=pospair_pal[m][ww+1]-1;
			} else {
			  pospair_pal[m][ww]=-1;
			}
		      }
		      pospair_pal[m][w[m]-1]=-1;
		    }
                } else if (0.5 < left_right && left_right <= 1) {//shift_left=increase_left+decrease_right
		  //cout << "motif " << m << " shifted to left" << endl;
		  conv_saver[m]--;
                    refpos[m]++;
                    for (int ww=w[m]-1; ww>=1; ww--) { // PN 2018/06/11 corrected bug here "w>1" -> "w>=1" 
                        IC[m][ww]=IC[m][ww-1];
                        for (int r=0; r<4; r++) {
                            prob[m][ww][r]=prob[m][ww-1][r];
                        }
                    }
                    IC[m][pos_new]=computeIC(theta_new);
                    for (int r=0; r<4; r++) {
                        prob[m][pos_new][r]=theta_new[r];//pos_new=0
                    }
		    if (Para.pal_activated && center_pal[m]>0) {
		      center_pal[m]+=2;
		      for (int ww=w[m]-1; ww>=1; ww--) {
			if (pospair_pal[m][ww-1]>=0) {
			  pospair_pal[m][ww]=pospair_pal[m][ww-1]+1;
			} else {
			  pospair_pal[m][ww]=-1;
			}
		      }
		      pospair_pal[m][0]=-1;
		    }
                }
            }
        }
    }
}

void Functions::compute_information_content (Para Para, double** IC, double*** prob, int* w) {
    for (int m=0; m<Para.M; m++) {
        fill_n(IC[m], Para.Wmax, 0);
        for (int i=0; i<w[m]; i++) {
            IC[m][i] = computeIC(prob[m][i]);//alpha
        }
    }
}
double Functions::computeIC(double* prob) {
    double ic=0;
    for (int i=0; i<4; i++) {
        ic += -0.5*log2(0.5) + prob[i]*log2(prob[i]);
    }
    return(ic);
}

void Functions::Motif_shrink (Para Para, double** IC, int* w, int* refpos, double*** prob, double IC_cut, int** position, int* conv_saver) {
  if (Para.pal_activated) {
    cerr << "Motif_shrink has not been implemented for palindromic motifs" << endl;
    exit(1);
  }
    cout<<"Motif_shrink"<<endl;
    for (int m=0; m<Para.M; m++) {
        int beg;
        int end;
        if (w[m]>10) {
            int start = 0;
            double pick = 0;
            double max = 0;
            for (int i=0; i<w[m]-(10-1); i++) {
                pick = 0;
                for (int j=0; j<10; j++) {
                    pick += IC[m][i+j];
                }
                if (pick > max) {
                    max = pick;
                    start = i;
                }
            }
            beg = start;
            end = start + (10-1);
        } else {
            beg = 0;
            end = w[m]-1;
        }
        cout<<m<<" "<<w[m]<<" "<<beg<<" "<<end<<endl;
        for (int i=0; i<w[m]; i++) {
            cout<<IC[m][i]<<" ";
        }
        cout<<endl;
        int counter=end;
        for (int i=beg; i<end; i++) {
            if (IC[m][i]<IC_cut && beg==i && (end+1-beg)>Para.W0) {
                beg++;
            }
            if ((IC[m][counter])<IC_cut && end==counter && (end+1-beg)>Para.W0) {
                end--;
            }
            counter--;
        }
        conv_saver[m]+=beg;
        w[m] = end-beg+1;
        int oldref = refpos[m];
        refpos[m] = (end-beg)/2;
        for (int n=0; n<Para.S; n++) {
            if (position[m][n]<Para.L) {
                position[m][n]=position[m][n]-oldref+beg+refpos[m];
            }
            if (position[m][n]<0 || position[m][n]>=Para.L) {
                position[m][n]=Para.L;
                cout<<"really!!"<<endl;
            }
        }
        for (int i=0; i<w[m]; i++) {
            IC[m][i]=IC[m][beg+i];
            for (int r=0; r<4; r++) {
                prob[m][i][r]=prob[m][beg+i][r];
            }
        }
        cout<<m<<" "<<w[m]<<" "<<beg<<" "<<end<<endl;
        for (int i=0; i<w[m]; i++) {
            cout<<IC[m][i]<<" ";
        }
        cout<<endl;
    }
}

    //=============================start_probit=======================

void Functions::sample_Z (double** Z, Para Para, gsl_rng* p, int** position, double** X, double** beta, int** T_probit, double*** Y_probit, int ncol_Y) {
    for (int m=0; m<Para.M; m++) {
        for (int n=0; n<Para.S; n++) {
            double mean = 0;
            for (int c=0; c<ncol_Y; c++) {
                if (T_probit[m][c] > 0) {
                    mean += Y_probit[m][c][n]*beta[m][c];
                }
            }
            if (position[m][n]==Para.L) { //we need -ive
                double trempo = - gsl_ran_gaussian_tail (p, mean, 1);
                Z[m][n] = trempo + mean;
            } else {
                double trempo = gsl_ran_gaussian_tail (p, -mean, 1);
                Z[m][n] = trempo + mean;
            }
        }
    }
}

void Functions::sample_beta (double** Z, Para Para, gsl_rng* p, double** beta, double variance_beta, int** T_probit, int ncol_Y, double intercept_mu, double intercept_variance, double*** Y_probit) {
    for (int m=0; m<Para.M; m++) {
        for (int c=0; c<ncol_Y; c++) {//here we need ncol_y
            if (T_probit[m][c]>0) {
                sample_beta_mk (Z,  Para, p, beta, variance_beta, T_probit, ncol_Y,intercept_mu,intercept_variance,Y_probit,m, c);
            }
        }
    }
}

void Functions::sample_beta_mk (double** Z, Para Para, gsl_rng* p, double** beta, double variance_beta, int** T_probit, int ncol_Y, double intercept_mu, double intercept_variance, double*** Y_probit,int m,int c)  {
    double one_over_variance = 1.0/variance_beta;
    if (c==0) {
        one_over_variance= 1.0/intercept_variance;
    }
    double mu_beta= 0;
    if (c==0) {
        mu_beta= intercept_mu;
    }
    double sum_x2 = 0;
    double numerator = 0;
    for (int n=0; n<Para.S; n++) {
        double sum_beta_x = 0;
        for (int d=0; d<ncol_Y ; d++) {
            if (c != d && T_probit[m][d] > 0) {
                sum_beta_x += beta[m][d]*Y_probit[m][d][n];
            }
        }
            numerator += Y_probit[m][c][n]*(Z[m][n]-sum_beta_x);
            sum_x2 += pow(Y_probit[m][c][n],2);
    }
    numerator += mu_beta*one_over_variance;
    double inverse_variance = sum_x2 + one_over_variance;
    double mu = numerator/inverse_variance;
    double trempi = gsl_ran_gaussian (p,sqrt(1.0/inverse_variance));
    beta[m][c] = trempi + mu;
}

void Functions::probit_dim_update (double** Z, Para Para, gsl_rng* p, double** X_vectors, double** beta, double variance_beta, int** T_probit, int numofvectors, double*** Y_probit, double** prior_cut, double** cut, int** Y_sorted, double intercept_mu, double intercept_variance, int* typeofvectors, int ncol_Y, int numoftrees, int** branch_countleaves, int*** X_tree_merge, double** merge_prior) {
    if (numofvectors>0) {
        int nattempts=ceil(numofvectors/10.0);
        for (int iattempt=0; iattempt< nattempts; iattempt++) {
            double log_prior_ratio_k = log(0.5);
            for (int m =0; m< Para.M; m++) {
                int t_marg= (int)gsl_rng_uniform_int(p, numofvectors)+1; //returns from 1 to #ofvectors (doesn't include the intercept)
                int active_pc=0;
                for (int c_temp=1; c_temp<numofvectors+1; c_temp++) {//count # of active components
                    if (T_probit[m][c_temp]>0) {
                        active_pc++; //aside from the intercept
                    }
                }
                double log_prior_T=0;
                if (T_probit[m][t_marg]>0) {
                    log_prior_T=log((double)(active_pc-1+1)/((double)(numofvectors-(active_pc-1))));
                } else {
                    log_prior_T=log((double)(active_pc+1)/((double)(numofvectors-(active_pc))));
                }
                double log_prior_activate = log_prior_ratio_k + log_prior_T;
                if (typeofvectors[t_marg-1]==1) { //treat as it is! Only activate or disactivate
                    probit_dim_vector_nocut ( Z, Para, p, X_vectors, beta, variance_beta, T_probit, Y_probit, m, t_marg, log_prior_activate, ncol_Y);
                } else if (typeofvectors[t_marg-1]==2) {//activate or disactivate and then we may binariez the data
                    probit_dim_vector_cut (Z, Para, p, X_vectors, beta, variance_beta, T_probit, Y_probit, prior_cut, cut, Y_sorted, m, t_marg, log_prior_activate, ncol_Y);
                }
                if (T_probit[m][t_marg]>0) {
                    sample_beta_mk (Z, Para, p, beta, variance_beta, T_probit, ncol_Y, intercept_mu, intercept_variance, Y_probit, m, t_marg);
                }
            }
        }
    }
    if (numoftrees>0) {
        double log_prior_ratio_k_tree = log(0.5);
        for (int m =0; m< Para.M; m++) {
            int choose_tree = (int)gsl_rng_uniform_int(p, numoftrees);
            int t_marg = 1+numofvectors+choose_tree;
            int active_tree=0;
            for (int itree=0; itree<numoftrees; itree++) {//count # of active components
                if (T_probit[m][1+numofvectors+itree]>0) {
                    active_tree++;
                }
            }
            double log_prior_T=0;
            if (T_probit[m][t_marg]>0) {
                log_prior_T=log((double)(active_tree-1+1)/((double)(numoftrees-(active_tree-1))));
            } else {
                log_prior_T=log((double)(active_tree+1)/((double)(numoftrees-(active_tree))));
            }
            double log_prior_activate = log_prior_ratio_k_tree + log_prior_T;
            probit_dim_tree (Z, Para, p, beta, variance_beta, T_probit, numofvectors, Y_probit, prior_cut, cut, branch_countleaves, X_tree_merge, merge_prior, ncol_Y, numoftrees, m, choose_tree, log_prior_activate);
            if (T_probit[m][t_marg]>0) {
                sample_beta_mk (Z, Para, p, beta, variance_beta, T_probit, ncol_Y, intercept_mu, intercept_variance, Y_probit, m, t_marg);
            }
        }
    }
}

    //=====
void Functions::probit_dim_vector_nocut (double** Z, Para Para, gsl_rng* p, double** X_vectors, double** beta, double variance_beta, int** T_probit, double*** Y_probit, int m, int t_marg, double log_prior_activate, int ncol_Y) {//activate or disactivate
    
    double one_over_variance=1/variance_beta;
    double sum_x2_raw = 0;
    double mid_expo_raw = 0;
    for (int n=0; n<Para.S; n++) {
        double sum_beta_y = 0;
        for (int d=0; d<ncol_Y ; d++) {
            if (T_probit[m][d] > 0 && d!=t_marg){
                sum_beta_y += (beta[m][d]*Y_probit[m][d][n]);
            }
        }
        mid_expo_raw += (Z[m][n]-sum_beta_y)*X_vectors[t_marg][n];//here //new
        sum_x2_raw += pow(X_vectors[t_marg][n],2);//here //new
    }
    double good_term_raw = one_over_variance+sum_x2_raw; //new
    double log_lik_ratio_raw = 0.5*log(one_over_variance) - 0.5*log(good_term_raw) + 0.5*pow(mid_expo_raw,2)/good_term_raw; //new
    double prob[2];
    fill_n(prob, 2, 0);
    prob[0]=1; //no_cut_prob
    prob[1]=exp(log_lik_ratio_raw+log_prior_activate);
    int choose = sample(prob, p, prob[0]+prob[1]);
    if (choose == 1){ //activate
        T_probit[m][t_marg]=1;
        for (int n=0; n< Para.S; n++) {//update Y_probit
            Y_probit[m][t_marg][n]=X_vectors[t_marg][n];
        }
    }
    else  if (choose == 0) {
        T_probit[m][t_marg]=0;
    }
}
    //=====

void Functions::probit_dim_vector_cut (double** Z, Para Para, gsl_rng* p, double** X_vectors, double** beta, double variance_beta, int** T_probit, double*** Y_probit, double** prior_cut, double** cut, int** Y_sorted, int m, int t_marg, double log_prior_activate, int ncol_Y) {//select cut
    
    double one_over_variance=1/variance_beta;
    double total_beta_y[Para.S];
    double total_beta=0;
    double sum_x2_raw = 0;
    double mid_expo_raw = 0;
    for (int n=0; n<Para.S; n++) {
        double sum_beta_y = 0;
        for (int d=0; d<ncol_Y ; d++) {
            if (T_probit[m][d] > 0 && d!=t_marg) {
                sum_beta_y += (beta[m][d]*Y_probit[m][d][n]);
            }
        }
        total_beta_y[n] = (Z[m][n]-sum_beta_y);
        total_beta += total_beta_y[n];
        mid_expo_raw += (Z[m][n]-sum_beta_y)*X_vectors[t_marg][n];//here //new
        sum_x2_raw += pow(X_vectors[t_marg][n],2);//here //new
    }
    double good_term_raw = one_over_variance+sum_x2_raw; //new
    double log_lik_ratio_raw = 0.5*log(one_over_variance) - 0.5*log(good_term_raw) + 0.5*pow(mid_expo_raw,2)/good_term_raw;//no_cut
    double prob_cut[Para.S+1];
    fill_n(prob_cut, Para.S+1, 0);
    prob_cut[Para.S-1]=1; //no_cut_prob
    double sum_cut= prob_cut[Para.S-1];
    double mid_expo_left=0;
    double mid_expo_right=total_beta;
    for (int n=0; n<Para.S-1; n++) { //for all the possible cuts
        int n_left=n+1;
        double C1 = -(double)(Para.S-n_left)/(double)Para.S;
        double C2 = 1.0+C1;
        double C1sqr = pow(C1, 2);
        double C2sqr = pow(C2, 2);
        double sum_y2=(double)n_left*C1sqr+(double)(Para.S-n_left)*C2sqr;
        mid_expo_left += total_beta_y[Y_sorted[t_marg][n]];
        mid_expo_right-= total_beta_y[Y_sorted[t_marg][n]];
        double mid_expo = mid_expo_left*C1+mid_expo_right*C2;
        double good_term = one_over_variance+sum_y2;
        double log_lik_ratio = 0.5*log(one_over_variance) - 0.5*log(good_term) + 0.5*pow(mid_expo,2)/good_term;//for the cut
        prob_cut[n] = 0.5*exp(log_lik_ratio+ log_prior_activate)*prior_cut[t_marg][n];
        sum_cut += prob_cut[n];
    }
    prob_cut[Para.S]=0.5*exp(log_lik_ratio_raw+ log_prior_activate);//0.5 to take into account that the probability of having a component as it is and cutting it will sum up to the same weight if we don't cut
    sum_cut += prob_cut[Para.S];
    int choose_cut = sample(prob_cut, p, sum_cut);
    cut[m][t_marg] = choose_cut;
    if (choose_cut<Para.S-1) { //activate and cut
        T_probit[m][t_marg]=2; //binary
        double cut_pc = (X_vectors[t_marg][Y_sorted[t_marg][choose_cut+1]]+X_vectors[t_marg][Y_sorted[t_marg][choose_cut]])/2.0;
        double C1 = -(double)(Para.S-(choose_cut+1))/(double)Para.S;
        for (int n=0; n< Para.S; n++) {//update Y_probit
            if (X_vectors[t_marg][n] <= cut_pc) {
                Y_probit[m][t_marg][n]=C1;
            } else {
                Y_probit[m][t_marg][n]=1.0+C1;
            }
        }
    } else if (choose_cut == (Para.S-1)) {//deactivate
        T_probit[m][t_marg]=0;
    } else if (choose_cut == Para.S) { //activate without a cut
        T_probit[m][t_marg]=1;
        for (int n=0; n< Para.S; n++) {//update Y_probit
            Y_probit[m][t_marg][n]=X_vectors[t_marg][n];
        }
    }
}

    //04_dec_2017############################
void Functions::probit_dim_tree (double** Z, Para Para, gsl_rng* p, double** beta, double variance_beta, int** T_probit, int numofvectors, double*** Y_probit, double** prior_cut, double** cut, int** branch_countleaves, int*** X_tree_merge, double** merge_prior, int ncol_Y, int numoftrees, int m, int choose_tree, double log_prior_activate) {
    
    double* total_beta_y= new double [Para.S];
    double* total_beta_merge = new double [Para.S-1];
    double one_over_variance=1.0/variance_beta;
    
    int t_marg = 1+numofvectors+choose_tree;
    
    double total_beta=0;
    for (int n=0; n<Para.S; n++) {
        double sum_beta_y = 0;
        for (int d=0; d<ncol_Y ; d++) {
            if (T_probit[m][d] > 0 && d!=t_marg){
                sum_beta_y += (beta[m][d]*Y_probit[m][d][n]);
            }
        }
        total_beta_y[n] = (Z[m][n]-sum_beta_y);
        total_beta += total_beta_y[n];
    }
    for (int n=0; n<Para.S-1; n++) {//for each possible merge
        double branch1=0;
        double branch2=0;
        if (X_tree_merge[choose_tree][n][0]>0) {
            branch1=total_beta_merge[X_tree_merge[choose_tree][n][0]-1];
        } else {
            branch1=total_beta_y[-X_tree_merge[choose_tree][n][0]-1];
        }
        if (X_tree_merge[choose_tree][n][1]>0) {
            branch2=total_beta_merge[X_tree_merge[choose_tree][n][1]-1];
        } else {
            branch2=total_beta_y[-X_tree_merge[choose_tree][n][1]-1];
        }
        total_beta_merge[n]=branch1+branch2;
    }
        //cout<<"total_beta "<<total_beta<<" "<<total_beta_merge[Para.S-2]<<endl;
    double prob_cut[Para.S];
    fill_n(prob_cut, Para.S, 0);
    prob_cut[Para.S-1]=1; //no_cut_prob
    double sum_cut= prob_cut[Para.S-1];
    
    for (int n=0; n<Para.S-2; n++) { //for all the possible merges except the root
        int n_left=branch_countleaves[choose_tree][n];
        double C1 = -(double)(Para.S-n_left)/(double)Para.S;
        double C2 = 1.0+C1;
        double C1sqr = pow(C1, 2);
        double C2sqr = pow(C2, 2);
        double sum_y2=(double)n_left*C1sqr+(double)(Para.S-n_left)*C2sqr;
        double mid_expo = total_beta_merge[n]*C1+(total_beta-total_beta_merge[n])*C2;
        double good_term = one_over_variance+sum_y2;
        double log_lik_ratio = 0.5*log(one_over_variance) - 0.5*log(good_term) + 0.5*pow(mid_expo,2)/good_term;
        prob_cut[n] = exp(log_lik_ratio+log_prior_activate)*merge_prior[choose_tree][n];
        sum_cut += prob_cut[n];
    }
    
    int choose_cut = sample(prob_cut, p, sum_cut);
    
    cut[m][t_marg] = choose_cut;
    
    if (choose_cut==Para.S-1) {
        T_probit[m][t_marg]=0;
    } else {
        double C1 = -(double)(Para.S-branch_countleaves[choose_tree][choose_cut])/(double)Para.S;
        
        fill_n(Y_probit[m][t_marg], Para.S, 1.0+C1);
        
        Set_Y_probit_tree(X_tree_merge,Y_probit, choose_cut, choose_tree, m, C1,t_marg);
        
        T_probit[m][t_marg]=3;
    }
    
    delete[] total_beta_y;
    delete[] total_beta_merge;
}

double Functions::calculate_prob_x (double*** prob, double** backgmodel_pos, Para Para, int** B, int** sequences, int** position, int* refpos, int** sequencesMbg) {
    double prob_x = 0;

    
    for (int n=0; n<Para.S; n++) {
        double tprob=0;
        for (int l=0; l<Para.L; l++) {
            if (B[n][l]==Para.M) {
                tprob += log(backgmodel_pos[l][sequencesMbg[n][l]]);
            } else {
                tprob += log(prob[B[n][l]][l-position[B[n][l]][n]+refpos[B[n][l]]][sequences[n][l]]);
            }
        }
        prob_x += tprob;
    }
    cout<< prob_x <<endl;
    return prob_x;
}

//#######################################


void Functions::Set_Y_probit_tree(int*** X_tree_merge,double*** Y_probit,int choose_cut,int choose_tree, int m, double C1, int t_marg) {
    if (X_tree_merge[choose_tree][choose_cut][0]>0) {
        Set_Y_probit_tree(X_tree_merge,Y_probit, X_tree_merge[choose_tree][choose_cut][0]-1, choose_tree, m, C1, t_marg);
    } else {
        Y_probit[m][t_marg][-X_tree_merge[choose_tree][choose_cut][0]-1]=C1;
    }
    if (X_tree_merge[choose_tree][choose_cut][1]>0) {
        Set_Y_probit_tree(X_tree_merge,Y_probit, X_tree_merge[choose_tree][choose_cut][1]-1, choose_tree, m, C1, t_marg);
    } else {
        Y_probit[m][t_marg][-X_tree_merge[choose_tree][choose_cut][1]-1]=C1;
    }
}
    //==17-12-2017==============================
void Functions::read_tree_covariate_file (ifstream &tree_covariate_stream, int &numoftrees, int*** X_tree_merge, double** X_tree_height, double** merge_prior, int** branch_countleaves, Para Para) {
    
    int minimum_count = 10; //mimi
    for (int n=0; n<Para.S-1; n++) {//reading treefile
        for (int k=0; k<numoftrees; k++) {
            for (int i=0; i<2; i++) {
                tree_covariate_stream>>X_tree_merge[k][n][i];
            }
            tree_covariate_stream>>X_tree_height[k][n];
        }
    }

    for (int k=0; k<numoftrees; k++) { //counting leaves
        for (int n=0; n<Para.S-1; n++) {//for each possible merge
            int branch1=0;
            int branch2=0;
            if (X_tree_merge[k][n][0]>0) {
                branch1=branch_countleaves[k][X_tree_merge[k][n][0]-1];
            } else {
                branch1=1;
            }
            if (X_tree_merge[k][n][1]>0) {
                branch2=branch_countleaves[k][X_tree_merge[k][n][1]-1];
            } else {
                branch2=1;
            }
            branch_countleaves[k][n]=branch1+branch2;
        }
        cout<<"total_leaves "<<branch_countleaves[k][Para.S-2]<<endl;
    }

    for (int k=0; k<numoftrees; k++) {
        for (int n=Para.S-2; n>=0; n--) {
            if (X_tree_merge[k][n][0]>0) {
                merge_prior[k][X_tree_merge[k][n][0]-1]=X_tree_height[k][n]-X_tree_height[k][X_tree_merge[k][n][0]-1];
            }
            if (X_tree_merge[k][n][1]>0) {
                merge_prior[k][X_tree_merge[k][n][1]-1]=X_tree_height[k][n]-X_tree_height[k][X_tree_merge[k][n][1]-1];
            }
        }
        double total_tree_dist=0;
        for (int n=0; n<Para.S-1; n++) {
            if (branch_countleaves[k][n]<minimum_count) {
                merge_prior[k][n]=0;
            }
            total_tree_dist+=merge_prior[k][n];
        }
        for (int n=Para.S-2; n>=0; n--) {
            merge_prior[k][n]=merge_prior[k][n]/total_tree_dist;
                //merge_prior[k][n]=1.0/(double)(Para.S-1);//remember!!
        }
    }
    cout<<"merge_prior "<<endl;
    for (int n=0; n<Para.S-1; n++) {
        cout<<merge_prior[0][n]<<" ";
    }
}

void Functions::read_vector_covariate_file (ifstream& vector_covariate_stream, int &numofvectors, double** X_vectors, Para Para) {
    for (int n=0; n<Para.S; n++) {//reading probit
        for (int c=1; c<numofvectors+1; c++) {
            vector_covariate_stream>>X_vectors[c][n];
            cout<<X_vectors[c][n]<<" ";
        }
        cout<<endl;
    }
}

void Functions::write_some_files (ofstream& myfile, ofstream& hisfile, ofstream& ourfile, ofstream& herfile, ofstream& yourfile,ofstream& cutfile, Para Para, int sweep, int* w, int* refpos, double*** prob, double* phi, int numofvectors, int* K, int** position, double** pos_prob, int rbackg, int** T_probit, double** beta, int ncol_Y, double** cut, int* conv_saver, double prob_x, int* center_pal, int** pospair_pal) {
    
    for (int m=0; m<Para.M; m++) { //PWMs
      int npairedcol_pal = 0;
      for (int i=0; i<w[m]; i++) {
	if (pospair_pal[m][i]>=0) {
	  npairedcol_pal++;
	}
      }
      ourfile<<sweep<<" "<<m<<" "<<w[m]<<" "<<refpos[m] << " " << (double)center_pal[m]/2.0 << " " << npairedcol_pal;
      hisfile<<"sweep = "<<sweep<<" motif = "<<m<<" w = "<<w[m]<<" refpos = "<<refpos[m]<<" centerpal = "<< (double)center_pal[m]/2.0 << " npairedcol_pal = " << npairedcol_pal <<endl;
        for (int i=0; i<Para.Wmax; i++) {
            if (i < w[m]) {
	      hisfile << pospair_pal[m][i];
                for (int r=0; r<4; r++) {
                    ourfile<<" "<<prob[m][i][r];//PWM_R.txt
                    hisfile<<" "<<prob[m][i][r];//PWM.txt
                }
                hisfile<<endl;
            } else {
                for (int r=0; r<4; r++) {
                    ourfile<<" "<<0;
                }
            }
        }
        ourfile<<endl;
        hisfile<<endl;
    }
    
    if (sweep%100==0) {
        for (int m=0; m<Para.M; m++) { //Position //list_of_genes
            yourfile<<sweep<<" "<<m;
            for (int n=0; n<Para.S; n++) {
                if (position[m][n]==Para.L) {
                    yourfile<<" "<<-1;
                } else {
                    yourfile<<" "<<position[m][n];
                }
            }
            yourfile<<endl;
        }
    }

    if (sweep%100==0) {
        for (int m=0; m<Para.M; m++) { //PDF
            herfile<<sweep<<" "<<m<<" "<<phi[m]<<" "<<K[m];
            for (int l=0; l<Para.L; l++) {
                herfile<<" "<<pos_prob[m][l];
            }
            herfile<<endl;
        }
    }
    

    for (int m=0; m<Para.M; m++) { //Parameters
        myfile<<sweep<<" "<<m<<" "<<K[m]<<" "<<phi[m]<<" "<<refpos[m]<<" "<<w[m]<<" "<<rbackg<<" "<<conv_saver[m]<<" "<<prob_x;
        for (int k=0; k<ncol_Y; k++) {
            myfile<<" "<<T_probit[m][k];
        }
        for (int k=0; k<ncol_Y; k++) {
            if (T_probit[m][k]>0) {
                myfile<<" "<<beta[m][k];
            } else {
                myfile<<" "<<0;
            }
        }
        cutfile<<sweep<<" "<<m;
        for (int k=0; k<ncol_Y; k++) {
            if (T_probit[m][k]>0) {
                cutfile<<" "<<cut[m][k];
            } else {
                cutfile<<" "<<0;
            }
        }
        myfile<<endl;
        cutfile<<endl;
    }
}

    ///==================end of the code=========

    ///=====================Begin_trash===========

void Functions::simulate_X (int** position, int** B, double*** prob, double** backgmodel_pos, Para& Para, gsl_rng* p, int** sequences, int* w, int* refpos, Sequences* seqs, int rbackg) {
    
    if (rbackg==1) {
        int codemax = (int)((pow(4,rbackg+2)-4.0)/3.0);//recent_23_05_2017
        int INC = codemax/4;//recent_23_05_2017
        double*** backgmatrix = new double** [Para.L];
        for (int i=0; i<Para.L; i++) {
            int pointcount=0;
            backgmatrix[i] = new double* [INC];
            for (int inc=0; inc<INC; inc++) {
                backgmatrix [i][inc] = new double [4];
                for (int r=0; r<4; r++) {
                    backgmatrix[i][inc][r]= backgmodel_pos[i][pointcount];
                    pointcount++;
                }
            }
        }
        for (int s=0; s<Para.S; s++) {
            for (int l=0; l<Para.L; l++) {
                if (B[s][l]<Para.M) {
                    int m = B[s][l];
                    sequences[s][l]=sample(prob[m][l-(position[m][s]-refpos[m])], p, 1.0);
                } else {
                    if (l==0) {
                        sequences[s][l]=sample(backgmatrix[l][0], p, 1.0);
                        
                    } else {
                        sequences[s][l]=sample (backgmatrix[l][sequences[s][l-1]+1], p, 1.0);//can be generalized
                    }//modify in the class
                }
            }
        }
        for (int i =0; i<Para.L; i++) {
            for (int inc=0; inc<INC; inc++) {
                delete [] backgmatrix [i][inc];
            }
            delete [] backgmatrix [i];
        }
        delete [] backgmatrix;
        seqs->replace_sequenceM0M1Mrmax(sequences);
    } else if (rbackg==0) {
        for (int s=0; s<Para.S; s++) {
            for (int l=0; l<Para.L; l++) {
                if (B[s][l]<Para.M) {
                    int m = B[s][l];
                    sequences[s][l]=sample(prob[m][l-(position[m][s]-refpos[m])], p, 1.0);
                } else {
                    sequences[s][l]=sample (backgmodel_pos[l], p, 1.0);
                }
            }
        }
        seqs->replace_sequenceM0M1Mrmax(sequences);
    } else {
        cout<<"Error"<<endl;
        exit(0);
    }
}
    //    //================
    //void Functions::simulate_X_pos (int** position, int** B, double*** prob, double** backgmodel_pos, Para& Para, gsl_rng* p, int** sequences, int* w, int* refpos, Sequences* seqs, int rbackg) {
    //    if (rbackg !=0 ) {// <== recent
    //
    //        int INC = 5; //can be generalized ==> Already_done
    //
    //        int codemax = (int)((pow(4,rbackg+2)-4.0)/3.0);//recent_23_05_2017
    //        INC = codemax/4;//recent_23_05_2017
    //
    //        double*** backgmatrix = new double** [Para.L];
    //        for (int i=0; i<Para.L; i++) {
    //            int pointcount=0;
    //            backgmatrix[i] = new double* [INC];
    //            for (int inc=0; inc<INC; inc++) {
    //                backgmatrix [i][inc] = new double [4];
    //                for (int r=0; r<4; r++) {
    //                    backgmatrix[i][inc][r]= backgmodel_pos[i][pointcount];
    //                    pointcount++;
    //                }
    //            }
    //        }
    //        for (int s=0; s<Para.S; s++) {
    //            for (int l=0; l<Para.L; l++) {
    //                if (B[s][l]<Para.M) {
    //                    int m = B[s][l];
    //                    sequences[s][l]=sample(prob[m][l-(position[m][s]-refpos[m])], p, 1.0);
    //                } else {
    //                        //===recent_23_05_2017
    //                    if (l==0) {
    //                        sequences[s][l] = sample(backgmatrix[l][0], p, 1.0);
    //                    } else if (l<rbackg) {
    //                        int pointo = 1;
    //                        for (int inc=1; inc<l+1; inc++) {
    //                            pointo += (sequences[s][l-inc]*pow(4, inc-1));
    //                        }
    //                        sequences[s][l] = sample(backgmatrix[l][pointo], p, 1.0);
    //                    } else {
    //                        int pointo = 1;
    //                        for (int inc=1; inc<rbackg+1; inc++) {
    //                            pointo += (sequences[s][l-inc]*pow(4, inc-1));
    //                        }
    //                        sequences[s][l] = sample(backgmatrix[l][pointo], p, 1.0);//can be generalized ==> already_done
    //                    }//modify in the class ==> Why?!
    //                }//===end_of_recent_23_05_2017
    //            }
    //        }
    //        for (int s=0; s<Para.S; s++) {
    //            for (int l=0; l<Para.L; l++) {
    //                cout<<sequences[s][l];
    //            }
    //            cout<<endl;
    //        }
    //        for (int i =0; i<Para.L; i++) {
    //            for (int inc=0; inc<INC; inc++) {
    //                delete [] backgmatrix [i][inc];
    //            }
    //            delete [] backgmatrix [i];
    //        }
    //        delete [] backgmatrix;
    //        seqs -> replace_sequenceM0M1Mrmax(sequences);
    //    } else if (rbackg==0) {//========================
    //
    //        for (int s=0; s<Para.S; s++) {
    //            for (int l=0; l<Para.L; l++) {
    //                if (B[s][l]<Para.M) {
    //                    int m = B[s][l];
    //                    sequences[s][l]=sample(prob[m][l-(position[m][s]-refpos[m])], p, 1.0);
    //                } else {
    //                    sequences[s][l]=sample (backgmodel_pos[l], p, 1.0);
    //                }
    //            }
    //        }
    //        seqs->replace_sequenceM0M1Mrmax(sequences);
    //    } else {//==========================
    //
    //        cout<<"Error"<<endl;
    //        exit(0);
    //    }
    //}

    //================
    //double Functions::CodeChar(char inc) {
    //    if        (inc=='A') {
    //        return(0);
    //    } else if (inc=='C') {
    //        return(1);
    //    } else if (inc=='G') {
    //        return(2);
    //    } else if (inc=='T') {
    //        return(3);
    //    }
    //    return(-1);
    //}

    //void sample_A (double*** prob,int** seq,int** sequencesMbg, int** pos, Para Para, gsl_rng* p, double* phi, int* w,int* refpos, double** pos_prob, double** backgmodel_pos, bool ICbg, double** IC) {
    //    int M=Para.M;
    //    int S=Para.S;
    //    int L=Para.L;
    //    double pr[L+1];
    //    double pr_motif[M];
    //    double pr_non[M];
    //    for (int m=0; m<M; m++) {
    //        pr_motif[m]= phi[m];
    //        pr_non[m]=1-phi[m];
    //        for (int s=0; s<S; s++) {
    //            double sum = 0;
    //            for(int i=0; i<L; i++) { //Position_likelihood_start_from_here
    //                pr[i] = pr_motif[m]*pos_prob[m][i];
    //                double pseq = 1;
    //                for(int r=0; r<w[m]; r++) {
    //                    int iposinseq=i-refpos[m]+r;
    //                    if (iposinseq>=0 && iposinseq<L) {
    //                        int xi=seq[s][iposinseq];
    //                        int xi_bg = sequencesMbg[s][iposinseq];
    //                        double intersec=0;
    //                        double temp_value=0;
    //                        for (int k=0; k<M; k++) {
    //                            if (k!=m) {
    //                                if (pos[k][s] < L&& pos[k][s]-refpos[k] <= iposinseq && iposinseq < pos[k][s]-refpos[k]+w[k]) {
    //                                    if (ICbg) {
    //                                        intersec += IC[k][iposinseq-(pos[k][s]-refpos[k])];
    //                                        temp_value+=prob[k][iposinseq-(pos[k][s]-refpos[k])][xi]*IC[k][iposinseq-(pos[k][s]-refpos[k])];
    //                                    } else {
    //                                        intersec++;
    //                                        temp_value+=prob[k][iposinseq-(pos[k][s]-refpos[k])][xi];
    //                                    }
    //                                }
    //                            }
    //                        }
    //                        double numerator;
    //                        if (ICbg) {
    //                            numerator = (temp_value+prob[m][r][xi]*IC[m][r])/(double)(intersec+IC[m][r]);
    //                        } else {
    //                            numerator = (temp_value+prob[m][r][xi])/(double)(intersec+1);
    //                        }
    //                        if (intersec > 0) {
    //                            pseq *= numerator/(temp_value/(double)intersec);
    //                        } else {
    //                            pseq *= numerator/backgmodel_pos[iposinseq][xi_bg];
    //                        }
    //                    }
    //                }
    //                pr[i] *= pseq;
    //                sum += pr[i];
    //            }
    //            pr[L]=pr_non[m];
    //            sum += pr_non[m];
    //            int rs = sample(pr, p, sum);
    //            pos[m][s]=rs;
    //        }
    //    }
    //}
    //
    //void sample_A_cluster (double*** prob,int** seq,int** sequencesMbg, int** pos, Para Para, gsl_rng* p, double* phi, int* w,int* refpos, double** pos_prob, double** backgmodel_pos, bool ICbg, double** IC, int* gene_member, double** phi_clust, int** I) {//cluster
    //    int M=Para.M;
    //    int S=Para.S;
    //    int L=Para.L;
    //    double pr[L+1];
    //    double pr_motif[M];
    //    double pr_non[M];
    //    int C= 50;
    //    for (int c=0; c<C; c++) {
    //        for (int m=0; m<M; m++) {
    //            if (I[m][c]==1) {
    //                pr_motif[m]= phi_clust[m][c];
    //                pr_non[m]= 1-phi_clust[m][c];
    //                for (int s=0; s<S; s++) {
    //                    if (gene_member[s]==c) {
    //                        double sum = 0;
    //                        for(int i=0; i<L; i++) { //Position_likelihood_start_from_here
    //                            pr[i] = pr_motif[m]*pos_prob[m][i];
    //                            double pseq = 1;
    //                            for(int r=0; r<w[m]; r++) {
    //                                int iposinseq=i-refpos[m]+r;
    //                                if (iposinseq>=0 && iposinseq<L) {
    //                                    int xi=seq[s][iposinseq];
    //                                    int xi_bg = sequencesMbg[s][iposinseq];
    //                                    double intersec=0;
    //                                    double temp_value=0;
    //                                    for (int k=0; k<M; k++) {
    //                                        if (k!=m) {
    //                                            if (pos[k][s] < L&& pos[k][s]-refpos[k] <= iposinseq && iposinseq < pos[k][s]-refpos[k]+w[k]) {
    //                                                if (ICbg) {
    //                                                    intersec += IC[k][iposinseq-(pos[k][s]-refpos[k])];
    //                                                    temp_value+=prob[k][iposinseq-(pos[k][s]-refpos[k])][xi]*IC[k][iposinseq-(pos[k][s]-refpos[k])];
    //                                                } else {
    //                                                    intersec++;
    //                                                    temp_value+=prob[k][iposinseq-(pos[k][s]-refpos[k])][xi];
    //                                                }
    //                                            }
    //                                        }
    //                                    }
    //                                    double numerator;
    //                                    if (ICbg) {
    //                                        numerator = (temp_value+prob[m][r][xi]*IC[m][r])/(double)(intersec+IC[m][r]);
    //                                    } else {
    //                                        numerator = (temp_value+prob[m][r][xi])/(double)(intersec+1);
    //                                    }
    //                                    if (intersec > 0) {
    //                                        pseq *= numerator/(temp_value/(double)intersec);
    //                                    } else {
    //                                        pseq *= numerator/backgmodel_pos[iposinseq][xi_bg];
    //                                    }
    //                                }
    //                            }
    //                            pr[i] *= pseq;
    //                            sum += pr[i];
    //                        }
    //                        pr[L]=pr_non[m];
    //                        sum += pr_non[m];
    //                        int rs = sample(pr, p, sum);
    //                        pos[m][s]=rs;
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}


    //void update_phi (gsl_rng* p, double* phi, int** pos, Para Para) {
    //    for (int m=0; m<Para.M; m++) {
    //        double non_motif = 0;
    //        for (int s=0; s<Para.S; s++) {
    //            if (pos[m][s] >= Para.L) {
    //                non_motif++;
    //            }
    //        }
    //            phi[m] = gsl_ran_beta(p, (double)Para.S-non_motif+1.0, (double)non_motif+100.0); // PN changed the prior to Beta(1, 10)
    //    }
    //}

    //void update_phi_clust (gsl_rng* p, double** phi_clust, int** pos, Para Para, int* gene_member, int* clustercount, int** I) {
    //    int C = 20;
    //    for (int c=0; c<C; c++) {
    //        double non_motif = 0;
    //        for (int m=0; m<Para.M; m++) {
    //            if (I[m][c]==1) {
    //                for (int s=0; s<Para.S; s++) {
    //                    if (gene_member[s]==c) {
    //                        if (pos[m][s] >= Para.L) {
    //                            non_motif++;
    //                        }
    //                    }
    //                }
    //                phi_clust[m][c] = gsl_ran_beta(p, (double)clustercount[c]-non_motif+1.0, (double)non_motif+1.0);
    //            }
    //        }
    //    }
    //}




    //=============begin_Clustering=========================
    //void clustering (gsl_rng* p, Para Para, int* clustercount, int* gene_member, int** pos, double** phi_clust, double* phi_init) {
    //    int CC = 20;
    //    int** motif_counter = new int* [Para.M];
    //    int** I = new int* [Para.M];
    //    for (int m=0; m<Para.M; m++) {
    //        for (int c=0; c<CC; c++) {
    //            motif_counter [m] = new int [CC];
    //            I [m] = new int [CC];
    //            for (int s=0; s<Para.S; s++) {
    //                if (pos[m][s] < Para.L) {
    //                    motif_counter[m][gene_member[s]]++;
    //                }
    //            }
    //            double phi_new, phi_old;
    //            if (I[m][c]==1) {
    //                phi_new = phi_init[m];
    //                phi_old = phi_clust[m][c];
    //            } else {
    //                phi_new = phi_clust[m][c];
    //                phi_old = phi_init[m];
    //            }
    //            double value_new = motif_counter[m][c]*log(phi_new)+(clustercount[c]-motif_counter[m][c])*log(1-phi_new);
    //            double value_old = motif_counter[m][c]*log(phi_old)+(clustercount[c]-motif_counter[m][c])*log(1-phi_old);
    //            double rando = gsl_rng_uniform(p); //to accept or reject the move
    //            double logpaccept = value_new-value_old;
    //            if (log(rando) < logpaccept) {
    //                I[m][c] = !I[m][c];
    //                cout<<value_new<<" "<<value_old<<endl;
    //            }
    //        }
    //    }
    //}
    //=============end_clustering========================
