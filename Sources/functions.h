/*
    name : functions.h

    This file is part of the source code of Multiple -- a program for
    de novo discovery of DNA motifs corresponding to Transcription
    Factor binding sites based on a probabilistic model of the DNA
    sequence that incorporates expression data as covariates.

    Copyright (C) 2018  Ibrahim Sultan

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING . If not, write to the
    Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Please send bugreports with examples or suggestions to pierre.nicolas@inra.fr
*/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_cdf.h>
#include <math.h>
#include <stdio.h>
#include "Sequences.h"

struct Para {
    int M;
    int S;
    int W0;
    int L;
    int P;
    double z;
    double z0;
    double z1;
    int Naccept;
    int Wmax;
  bool ICbg;
  bool pal_activated;
  double ispal_prior;
  double ispaired_inpal_prior;
  int min_halfpal;
  double** prior_centerpal;
};

class Functions {
    
public:
    double CodeChar(char);
    
    int sample(double* pr, gsl_rng* p,double);
    
    void sample_B(int** seq, int** pos, int* refpos, double*** prob, int** B, struct Para, gsl_rng* p, int* w, double** IC, bool ICbg);
    
    void sample_A(double*** prob,int** seq,int** sequencesMbg, int** pos, struct Para, gsl_rng* p, double* phi, int* w,int* refpos, double** pos_prob, double** backgmodel_pos, bool ICbg, double** IC);
    
    void count_theta(int*** motifs, int** seq, int** pos, int** B, struct Para, int* w, int* refpos);

  double integrate_dirichlet4_pobs(double alpha, int* countobs);

  double integrate_dirichlet4_pobs_mergedpal(double alpha, int* countobs1, int* countobs2);
  
  void update_pal(int*** motifs, Para& Para, gsl_rng* p, int* w, int* center_pal, int** pospair_pal);

    void update_theta(int*** motifs, double*** prob, struct Para, gsl_rng* p, int* w, double** IC, double** IC_temp, int** pos, int* refpos, double*** prob_temp, bool ICbg, int** B, int* center_pal, int** pospair_pal);
    
    void count_Q(int* K,int** D, double** Q, struct Para, int** pos_counter);
    
    void update_Gamma(int** D, double** Q, double** Gamma, struct Para, gsl_rng* p, int* K);
    
    void sample_D(int** D, int** temp_D, struct Para, int* K, gsl_rng* p, double** Q, double** temp_Q, int** pos_counter, int** point_detector);
    
    void update_K(int** D, int** temp_D, struct Para& Para, int* K, gsl_rng* p, double** Q, double** temp_Q, int** pos_counter, int** point_detector, double geo_prior_breakpoints);
    
    void update_w(int** seq, int** pos, double*** prob, Para& Para, gsl_rng* p,double** backgmodel_pos,int** sequencesMbg, int* refpos, int* w, bool ICbg, double** IC, double width_geo_prior, int* conv_saver, int wmin_geo_prior, int* center_pal, int** pospair_pal);
    
    void Matrix_change(struct Para, int** position, int** pos_counter);
    
    void count_Q_onemotif(struct Para, int* pos_counter_motif, int* D_motif, double* Q_motif, int km);
    
    void cal_pos_prob (int** D, int* K, double** Gamma, struct Para& Para, double** pos_prob, int** point_detector);
    
    void update_phi (gsl_rng* p, double* phi, int** pos, struct Para);
    
    void simulate_X (int** position, int** B, double*** prob, double** backgmodel_pos, struct Para& Para, gsl_rng* p, int** sequences, int* w, int* refpos, Sequences* seqs, int rbackg);
    
    void simulate_X_pos (int** position, int** B, double*** prob, double** backgmodel_pos, Para& Para, gsl_rng* p, int** sequences, int* w, int* refpos, Sequences* seqs, int rbackg);
    
    void update_refpos (int** D, Para& Para, int* K, gsl_rng* p, int* refpos, int** position, int* w, int** point_detector, double** pos_prob, int** pos_counter);
    
    void Update_BackgroundMr_position (int** sequencesMbg, int** B, Para& Para, int rbackg, gsl_rng* p, double** backgmodel_pos, bool positionwisebg);
    
    void Update_Background_order (gsl_rng* p, int &rbackg, int rbackmax,  Para& Para, int** B, int** &sequencesMbg, Sequences* seqs, bool positionwisebg); // PN added this function to encapsulate the update of pos and nopos back order
    
    void Update_Background_order_nopos (gsl_rng* p, int &rbackg, int rbackmax,  Para& Para, int** B, int** &sequencesMbg, Sequences* seqs);
    
    void Update_Background_order_pos (gsl_rng* p, int &rbackg, int rbackmax,  Para& Para, int** B, int** &sequencesMbg, Sequences* seqs);
    
    void Shift_motif (gsl_rng* p, Para Para, int* w, double** pos_prob, int** pos, int* refpos, int** seq, int** sequencesMbg, double*** prob, double** backgmodel_pos, bool ICbg, double** IC, int* conv_saver, int* center_pal, int** pospair_pal);
    
    void compute_information_content (Para Para, double** IC, double*** prob, int* w);
    
    double computeIC(double* prob);
    
    void Motif_shrink (Para Para, double** IC, int* w, int* refpos, double*** prob, double IC_cut, int** position, int* conv_saver);
    
    void sample_A_cluster (double*** prob,int** seq,int** sequencesMbg, int** pos, Para Para, gsl_rng* p, double* phi, int* w,int* refpos, double** pos_prob, double** backgmodel_pos, bool ICbg, double** IC, int* gene_member, double** phi_clust, int** I);
    
    void update_phi_clust (gsl_rng* p, double** phi_clust, int** pos, Para Para, int* gene_member, int* clustercount, int** I);
    
    void sample_Z (double** Z, Para Para, gsl_rng* p, int** position, double** X, double** beta, int** T_probit, double*** Y_probit, int ncol_Y);
    
    void sample_beta (double** Z, Para Para, gsl_rng* p, double** beta, double variance_beta, int** T_probit, int ncol_Y, double intercept_mu, double intercept_variance, double*** Y_probit);
    
    void sample_A_probit (double*** prob,int** seq,int** sequencesMbg, int** pos, Para Para, gsl_rng* p, int* w,int* refpos, double** pos_prob, double** backgmodel_pos, bool ICbg, double** IC, double** beta, int** T_probit, int numofvectors, double*** Y_probit, double** X, int ncol_Y, bool validate );
    
    void probit_dim_vector_cut (double** Z, Para Para, gsl_rng* p, double** X_vectors, double** beta, double variance_beta, int** T_probit, double*** Y_probit, double** prior_cut, double** cut, int** Y_sorted, int m, int t_marg, double log_prior_activate, int ncol_Y);
    
    void probit_dim_vector_nocut (double** Z, Para Para, gsl_rng* p, double** X_vectors, double** beta, double variance_beta, int** T_probit, double*** Y_probit, int m, int t_marg, double log_prior_activate, int ncol_Y);
    
    void probit_dim_tree (double** Z, Para Para, gsl_rng* p, double** beta, double variance_beta, int** T_probit, int numofvectors, double*** Y_probit, double** prior_cut, double** cut, int** branch_countleaves, int*** X_tree_merge, double** merge_prior, int ncol_Y, int numoftrees, int m, int choose_tree, double log_prior_activate);
    
    void sample_beta_mk (double** Z, Para Para, gsl_rng* p, double** beta, double variance_beta, int** T_probit, int ncol_Y, double intercept_mu, double intercept_variance, double*** Y_probit,int m,int c);
    
    double tree_sub (int*** X_tree,double* total_beta_y, int n, int t_marg);
    
    void Set_Y_probit_tree (int*** X_tree_merge,double*** Y_probit,int choose_cut,int choose_tree, int m, double C1, int t_marg);
    
    void probit_dim_update (double** Z, Para Para, gsl_rng* p, double** X_vectors, double** beta, double variance_beta, int** T_probit, int numofvectors, double*** Y_probit, double** prior_cut, double** cut, int** Y_sorted, double intercept_mu, double intercept_variance, int* typeofvectors, int ncol_Y, int numoftrees, int** branch_countleaves, int*** X_tree_merge, double** merge_prior);
    
    void read_tree_covariate_file (ifstream& tree_covariate_stream, int &numoftrees, int*** X_tree_merge, double** X_tree_height, double** merge_prior, int** branch_countleaves, Para Para);
    
    void read_vector_covariate_file (ifstream& vector_covariate_stream, int &numofvectors, double** X_vectors, Para Para);
    
    void write_some_files (ofstream& myfile, ofstream& hisfile, ofstream& ourfile, ofstream& herfile, ofstream& yourfile,ofstream& cutfile, Para Para, int sweep, int* w, int* refpos, double*** prob, double* phi, int numofvectors, int* K, int** position, double** pos_prob, int rbackg, int** T_probit, double** beta, int ncol_Y, double** cut, int* conv_saver, double prob_x, int* center_pal, int** pospair_pal);
    
        //void Y_probit_tree_col(int*** X_tree_merge,double*** Y_probit,int choose_cut,int t_marg, int m, int C1,int col);
    
    double calculate_prob_x (double*** prob, double** backgmodel_pos, Para Para, int** B, int** sequences, int** position, int* refpos, int** sequencesMbg);

  void initialize_prior_centerpal(Para& para, double alpha_centerpal);
};

#endif /* FUNCTIONS_H */
