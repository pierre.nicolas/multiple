/*
    name : Sequence.cc

    This file is part of the source code of Multiple -- a program for
    de novo discovery of DNA motifs corresponding to Transcription
    Factor binding sites based on a probabilistic model of the DNA
    sequence that incorporates expression data as covariates.
 
    Copyright (C) 2011  Pierre Nicolas

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING . If not, write to the
    Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Please send bugreports with examples or suggestions to pierre.nicolas@inra.fr
*/

#include <list>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <cmath>
using namespace std;

#include "Sequences.h"

Sequences::Sequences(const char* file, int rmax)
{


  ifstream ifile(file);
  string word;

  if ( ! ifile.good() )
    {
      cerr << "Cannot open the sequence list file:" << endl
	   << "\t\"" << file << "\"" << endl;
      exit(1);
    }

  ifile >> word;
  _nseq = atoi(word.c_str());
  ifile >> word;
  _lseq = atoi(word.c_str());
  cout << "nseq " << _nseq << " lseq " << _lseq << endl;

  _rmax = rmax;
  _seqid = new string [_nseq];
  _sequences = new char* [_nseq];
  for (int i=0; i<_nseq; i++) {
    ifile >> word;
    _seqid[i] = word;
    _sequences[i] = new char [_lseq];
    ifile >> word;
    istringstream ins(word);
    for (int t=0; t<_lseq; t++) {
      ins >> _sequences[i][t];
    }
  }
  ifile.close();
  
  _sequencesM0 = new int* [_nseq];
  _sequencesM1 = new int* [_nseq];
  for (int i=0; i<_nseq; i++) {
    _sequencesM0[i] = new int [_lseq];
    _sequencesM1[i] = new int [_lseq];
    for (int t=0; t<_lseq; t++) {
      _sequencesM0[i][t]=this->CodeChar(_sequences[i][t]);
    }
    _sequencesM1[i][0]=_sequencesM0[i][0];
    for (int t=1; t<_lseq; t++) {
      _sequencesM1[i][t]=4
	+this->CodeChar(_sequences[i][t-1])*4
	+this->CodeChar(_sequences[i][t]);
    }    
  }
  _sequencesM0Mrmax = new int** [_rmax+1];
  for (int r=0; r<=_rmax; r++) {
    _sequencesM0Mrmax[r] = new int* [_nseq];
    for (int i=0; i<_nseq; i++) {
      _sequencesM0Mrmax[r][i] = new int [_lseq];
    }    
  }  
  this->ComputeM0Mrmax();
} 

Sequences::~Sequences()
{
  // PN implemented Sequences::~Sequences()
  for (int i=0; i<_nseq; i++) {
      delete [] _sequences[i];
      delete [] _sequencesM0[i];
      delete [] _sequencesM1[i];
  }
  delete [] _seqid;
  delete [] _sequences;
  delete [] _sequencesM0;
  delete [] _sequencesM1;
  for (int r=0; r<=_rmax; r++) {
    for (int i=0; i<_nseq; i++) {
     delete []  _sequencesM0Mrmax[r][i];
    }    
    delete [] _sequencesM0Mrmax[r];
  }
  delete [] _sequencesM0Mrmax;

}

void Sequences::replace_sequenceM0M1Mrmax(int **seq)
{
   for (int i=0; i<_nseq; i++) {
        for (int t=0; t<_lseq; t++) {
            _sequencesM0[i][t]=seq[i][t];
        }
        _sequencesM1[i][0]=_sequencesM0[i][0];
        for (int t=1; t<_lseq; t++) {
            _sequencesM1[i][t]=4
            +seq[i][t-1]*4
            +seq[i][t];
        }
    }
    this->ComputeM0Mrmax();
}

void Sequences::ComputeM0Mrmax()
{
  for (int i=0; i<_nseq; i++) {
    for (int t=0; t<_lseq; t++) {   
      _sequencesM0Mrmax[0][i][t] = _sequencesM0[i][t];
    }    
  }
  for (int r=1; r<=_rmax; r++) {
    for (int i=0; i<_nseq; i++) {   
      for (int t=0; t<_lseq; t++) {   
	_sequencesM0Mrmax[r][i][t] = _sequencesM0Mrmax[r-1][i][t];
	if (t>=r) {
	  _sequencesM0Mrmax[r][i][t] += _sequencesM0Mrmax[0][i][t-r]*(int)pow(4,r);
	}
      }        
    }  
  }
  for (int r=1; r<=_rmax; r++) {
    for (int i=0; i<_nseq; i++) {   
      for (int t=0; t<_lseq; t++) {
	int lsuff = r;
	if (t<r) {
	  lsuff=t;
	}
	int codemaxinfr = (int)((pow(4,lsuff+1)-4.0)/3.0);
	_sequencesM0Mrmax[r][i][t] += codemaxinfr;
      }        
    }  
  }

//   for (int i=0; i<_nseq; i++) {   
//     cout << _seqid[i] << endl;
//     for (int r=0; r<=_rmax; r++) {
//       cout << "r=" << r;
//       for (int t=0; t<_lseq; t++) {   
// 	cout << " " << _sequencesM0Mrmax[r][i][t];
//       }
//       cout << endl;
//     }
//   }
}

int Sequences::CodeChar(char inc)
{
  if (inc=='A') { 
    return(0);
  } else if (inc=='C') {
    return(1);
  } else if (inc=='G') { 
    return(2);
  } else if (inc=='T') { 
    return(3);
  }
  return(-1);  
}

string Sequences::SubSeq(int seq_i, int tbegin, int tend)
{
  string word="";
  word.append(_sequences[seq_i]+tbegin, tend-tbegin+1);
  return(word);
}
