/*
    name : Sequence.h

    This file is part of the source code of Multiple -- a program for
    de novo discovery of DNA motifs corresponding to Transcription
    Factor binding sites based on a probabilistic model of the DNA
    sequence that incorporates expression data as covariates.
 
    Copyright (C) 2011  Pierre Nicolas

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING . If not, write to the
    Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Please send bugreports with examples or suggestions to pierre.nicolas@inra.fr
*/

#ifndef SEQUENCES_H
#define SEQUENCES_H

#include <string>
using namespace std;



class Sequences
{
private:
  int _nseq;
  int _lseq;
  int _rmax;
  string* _seqid;
  char** _sequences;
  int** _sequencesM0;
  int** _sequencesM1;
  int*** _sequencesM0Mrmax;
  int** sequencesM0();
  int** sequencesM1();

public:
  Sequences(const char* file, int rmax);
  ~Sequences(); 
  int NbSeq();
  int LeSeq();
  int CodeChar(char inc);
  int** sequencesMr(int r);
  string SubSeq(int seq_i, int tbegin, int tend);
  string SeqId(int seq_i);
  void ComputeM0Mrmax();
  void replace_sequenceM0M1Mrmax(int** seq);

};



inline int** Sequences::sequencesM0()
{
  return(_sequencesM0);
}
inline int** Sequences::sequencesM1()
{
  return(_sequencesM1);
}
inline int** Sequences::sequencesMr(int r)
{
  return(_sequencesM0Mrmax[r]);
}

inline string Sequences::SeqId(int seq_i)
{
  return(_seqid[seq_i]);
}


inline int Sequences::NbSeq()
{
  return(_nseq);
}

inline int Sequences::LeSeq()
{
  return(_lseq);
}


#endif /* SEQUENCES_H */
