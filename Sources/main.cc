/*
    name : main.cc

    This file is part of the source code of Multiple -- a program for
    de novo discovery of DNA motifs corresponding to Transcription
    Factor binding sites based on a probabilistic model of the DNA
    sequence that incorporates expression data as covariates.
 
    Copyright (C) 2018  Ibrahim Sultan

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING . If not, write to the
    Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Please send bugreports with examples or suggestions to pierre.nicolas@inra.fr
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_sort_vector.h>
#include <math.h>
#include "functions.h"
#include "Sequences.h"
#include <iomanip>


using namespace std;

int main(int argc, char *argv[])
{
    int Nsweep = 50000;
    int M = 2;
    double z = 0.25;
    double z0 = 0.25;
    double z1 = 1;
    int w_init = 5;
    int w0 = 3;
    int wmax = 25;
    int T = 20;
    bool positionwisebg = false;
    bool ICbg = false;
    bool validate = false;
    bool TSS = true;
    int wmin_geo_prior = w0+3;
    double width_geo_prior = 1; // 1 for uniform belongs to ]0,1] (ex. 0.9 for geometric)
    unsigned int rngseed = 2;
    double geo_prior_breakpoints = 0.25;
    double variance_beta = 1;
    double intercept_variance= 0.2;
    double intercept_mu = gsl_cdf_ugaussian_Pinv (0.01);
    
    string seqfile;
    string vector_covariate_filename="";
    string tree_covariate_filename="";
    string prefix;
    int num_arg = 1;
    bool pal_activated = false;
    double ispal_prior = 0.5;
    double ispaired_inpal_prior = 0.5;
    int min_halfpal = 1;
    double alpha_centerpal = 0.5;

    while (num_arg < argc) {
        cout<<"num_arg: "<<num_arg<<endl;
        if ( (strcmp (argv[num_arg], "-seq") == 0) && (num_arg + 1 < argc) ) { //seq file
            num_arg++;
            seqfile = argv[num_arg];
            cout << "seqfile: " << seqfile << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-xvectors") == 0) ) { //probit file
            num_arg++;
            vector_covariate_filename = argv[num_arg];
            cout << "vector_covariate_filename: " << vector_covariate_filename << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-xtrees") == 0) ) { //probit file
            num_arg++;
            tree_covariate_filename = argv[num_arg];
            cout << "tree_covariate_filename: " << tree_covariate_filename << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-prefix") == 0) ) { //probit file
            num_arg++;
            prefix = argv[num_arg];
            cout << "prefix: " << prefix << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-nmotifs") == 0) //# of motifs
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            M = atoi(argv[num_arg]);
            cout << "nmotifs: " << M << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-wmin") == 0) //minimum width
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            w0 = atoi(argv[num_arg]);
            cout << "wmin: " << w0 << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-wmax") == 0) //maximum width
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            wmax = atoi(argv[num_arg]);
            cout << "wmax: " << wmax << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-breakpmax") == 0) //max # of breakpoints
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            T = atoi(argv[num_arg]);
            cout << "breakpmax: " << T << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-Nsweep") == 0) //# sweeps
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            Nsweep = atoi(argv[num_arg]);
            cout << "Nsweep: " << Nsweep << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-rngseed") == 0) // PN add
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            rngseed = atoi(argv[num_arg]);
            cout << "rngseed: " << rngseed << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-ICbg") == 0) //IC (yes or no)
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            ICbg = atoi(argv[num_arg]);
            cout << "ICbg: " << ICbg << endl;
            num_arg++;
            
        } else if ( (strcmp (argv[num_arg], "-widthbg") == 0) // geometric width or uniform
                   && (num_arg + 2 < argc) ) {
            
            num_arg++;
            wmin_geo_prior = atoi(argv[num_arg]);
            cout << "wmin_geo_prior: " << wmin_geo_prior << endl;
            num_arg++;
            width_geo_prior = atof(argv[num_arg]);
            cout << "widthbg: " << width_geo_prior << endl;
            num_arg++;
            
        } else if (strcmp (argv[num_arg], "-validate") == 0) { // validate or no
            
            num_arg++;
            validate = true;
            cout << "validate: " << validate << endl;
            
        } else if ( (strcmp (argv[num_arg], "-TSS") == 0) // validate or no
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            TSS = false;
            cout << "TSS: " << TSS << endl;
            
        } else if ( (strcmp (argv[num_arg], "-geo_prior_breakpoints") == 0) // geo_prior_breakpoints
                   && (num_arg + 1 < argc) ) {
            
            num_arg++;
            geo_prior_breakpoints = atof(argv[num_arg]);
            num_arg++;
            
            cout << "geo_prior_breakpoints: " << geo_prior_breakpoints << endl;
        } else if ( (strcmp (argv[num_arg], "-z") == 0) // prior for the dirichlet distribution
                   && (num_arg + 3 < argc) ) {
            
            num_arg++;
            z = atof(argv[num_arg]);
            num_arg++;
            z0 = atof(argv[num_arg]);
            num_arg++;
            z1 = atof(argv[num_arg]);
            num_arg++;
            
            cout << "z: "<<z<<" "<<z0<<" "<<z1<<endl;
            
        } else if ( (strcmp (argv[num_arg], "-probit") == 0) //probit parameters
                   && (num_arg + 3 < argc) ) {
            
            num_arg++;
            variance_beta  = atof(argv[num_arg]);
            num_arg++;
            intercept_variance = atof(argv[num_arg]);
            num_arg++;
            double bridge = atof(argv[num_arg]);
            intercept_mu = gsl_cdf_ugaussian_Pinv(bridge);
            num_arg++;
            
            cout << "probit: " << variance_beta <<" "<<intercept_variance<<" "<<intercept_mu<< endl;
            
        } else if ( (strcmp (argv[num_arg], "-palprior") == 0) //prior for palindromic motifs
                   && (num_arg + 4 < argc) ) {
            
            num_arg++;
            ispal_prior  = atof(argv[num_arg]);
            num_arg++;
            ispaired_inpal_prior = atof(argv[num_arg]);
            num_arg++;
	    min_halfpal = atoi(argv[num_arg]);
            num_arg++;
	    alpha_centerpal = atof(argv[num_arg]);
	    num_arg++;
	    if (ispal_prior>0) {
	      pal_activated = true;
	    }
            cout << "prior for palindromic motifs: ispal_prior " << ispal_prior <<" ispaired_inpal_prior "<< ispaired_inpal_prior << " min_halfpal " << min_halfpal << " alpha_centerpal " << alpha_centerpal << endl;
            
        } else {            
            cerr << "Usage : " << argv[0] << " " << "-seq <file> [-nmotifs (default 1)]" << endl
            << "         [-wmin <#(default 5)>] [-wmax <#(default 30)>]" << endl
            << "         [-breakpmax <double(default 0)>] [-rseed <#>] [-Nsweep <#(default 1M)>] [-rngseed <#(default 0)>]" << endl;
            return (1) ;
        }
    }
    if (seqfile.length()==0) {
        cerr << "Usage : " << argv[0] << " " << "-seq <file> [-nmotifs (default 1)]" << endl
        << "         [-wmin <#(default 5)>] [-wmax <#(default 30)>]" << endl
        << "         [-breakpmax <double(default 0)>] [-rseed <#>] [-Nsweep <#(default 1M)>] [-rngseed <#(default 0)>]" << endl;
        return (1) ;
    }

    if (pal_activated && ICbg) {
      cerr << "Error: Unable to allow palindromic motifs when taking into account IC in motif overlap" << endl;
      cerr << "Error: Options -palprior and -ICbg are not compatible" << endl;
      exit(1);
    }
    
    Functions TF;
    int rbackg = 0;
    int rbackmax = 6;
    Sequences* seqs = new Sequences(seqfile.c_str(), rbackmax);
    int** sequences = seqs->sequencesMr(0);
    int** sequencesMbg = seqs->sequencesMr(rbackg);
    int L=seqs->LeSeq();
    int N=seqs->NbSeq();
    Para Para = {M,N,w0,L,T,z,z0,z1,Nsweep,wmax,ICbg,pal_activated,ispal_prior,ispaired_inpal_prior,min_halfpal}; //parameters
    TF.initialize_prior_centerpal(Para,alpha_centerpal);
    
    int conv_saver[M];
    fill_n(conv_saver, M, 0);
    int w[M]; // PN moved these to allocate memory after reading M in command line
    int K[M]; // PN moved these to allocate memory after reading M in command line
    int K0 = T+1;
    gsl_rng* p = gsl_rng_alloc(gsl_rng_taus);
    gsl_rng_set(p, rngseed);
    ofstream myfile;
    ofstream hisfile;
    ofstream ourfile;
    ofstream herfile;
    ofstream yourfile;
    ofstream cutfile;
    
    myfile.open((prefix+"parameters.txt").c_str());
    hisfile.open((prefix+"PWM.txt").c_str());
    ourfile.open((prefix+"PWM_R.txt").c_str());
    herfile.open((prefix+"position_pdf.txt").c_str());
    yourfile.open((prefix+"motif_sequence.txt").c_str());
    cutfile.open((prefix+"cutfile.txt").c_str());
    
    int refpos[M];
    double phi[M];
    fill_n(phi, M, 0.1);
    double** IC = new double* [M];
    double** IC_temp = new double* [M];
    for (int m=0; m<M; m++) {
        IC[m] = new double [wmax];
        IC_temp[m] = new double [wmax];
    }
    int** point_detector = new int* [M];
    int** position = new int* [M];
    int** pos_counter=new int* [M];
    int** D = new int* [M];
    int** temp_D = new int* [M];
    double** pos_prob = new double* [M];
    double** Q = new double* [M];
    double** temp_Q = new double* [M];
    double** Gamma = new double* [M];
    for (int m=0; m<M; m++) {
        pos_counter[m]=new int [L];
        pos_prob[m]= new double [L];
        point_detector[m] = new int [Para.P];
        fill_n(pos_prob[m], Para.L, double(1.0/Para.L));
        position[m] = new int [N];
        D[m] = new int [L-1];
        temp_D[m]= new int [L-1];
        fill_n(D[m], Para.L-1, 0);
        fill_n(temp_D[m], Para.L-1, 0);
        Q[m]=new double [K0];
        temp_Q[m]=new double [K0];
        Gamma[m]=new double [K0];
        for (int k=0; k<N; k++) {
            double randoo = gsl_rng_uniform(p);
            if(randoo < (double)10/(double)N) {
                position[m][k] = (int)gsl_rng_uniform_int(p, L);
            } else {
                position[m][k]=L;
            }
        }
    }
    int d;
    for (int m=0; m<M; m++) {
        K[m]=1;
        for (int t=0; t<K[m]-1; t++) {
            d=(int)gsl_rng_uniform_int(p, L-1);
            while (D[m][d]==1) {
                d=(int)gsl_rng_uniform_int(p, L-1);
            }
            D[m][d]=1;
        }
        for (int i=0; i<K0; i++) {
            Q[m][i]=1;
            temp_Q[m][i]=1;
            Gamma[m][i]=1.0/(double)(K0);
        }
    }
    double ones[4] ={1.0,1.0,1.0,1.0};
    int codemax = (int)((pow(4,rbackmax+2)-4.0)/3.0);
    double** backgmodel_pos = new double* [Para.L];
    for (int x=0; x<Para.L; x++) {
        backgmodel_pos [x]= new double [codemax];
        fill_n (backgmodel_pos[x], codemax, 0.25);
        int INC = codemax/4;
        for (int inc = 0; inc<INC ; inc++) {
            gsl_ran_dirichlet(p, 4, ones, backgmodel_pos[x]+4*inc);
        }
    }
    int*** motifs = new int**[M]; //here will be stored the counts that are used for updates
    for (int j=0; j<M; j++) {
        motifs[j]= new int* [wmax];
        for (int k=0; k<wmax; k++) {
            motifs[j][k]=new int [4];
        }
    }
    double*** prob = new double**[M];//PWM
    double*** prob_temp = new double**[M];
    int center_pal[M]; // PN add for palindromic motifs
    int** pospair_pal = new int* [M]; // PN add for palindromic motifs 
    for (int j=0; j<M; j++) {
      center_pal[j]=-1; // not a palindromic motif
      prob[j] = new double* [wmax];
      prob_temp[j] = new double* [wmax];
      pospair_pal[j] = new int [wmax];
      for (int k=0; k<wmax; k++) {
	pospair_pal[j][k]=-1; // positions are not paired since not a palindromic motif
	prob[j][k] = new double[4];
	prob_temp[j][k] = new double[4];
	gsl_ran_dirichlet (p, 4, ones, prob[j][k]);
      }
    }
    int** B = new int* [N];
    for (int j=0; j<N; j++) {
        B[j]= new int [L];
        for (int l=0; l<L; l++) {
            B[j][l]=M; //not the best intialization! better set them to 0!
        }
    }
    for (int m=0; m<M; m++) {
        w[m]= w_init;
        refpos[m]=w[m]/2;
        cout<<w[m]<<" "<<refpos[m]<<endl;
    }
        //==============vector_covariate_file========================
    int numofvectors = 0;//total
    int* typeofvectors=NULL;
    double** X_vectors=NULL;
    if (vector_covariate_filename.length()>0) {
        ifstream vector_covariate_stream (vector_covariate_filename.c_str());
        vector_covariate_stream >> numofvectors;
        typeofvectors = new int[numofvectors];
        for (int v=0; v<numofvectors; v++) {
            vector_covariate_stream >> typeofvectors[v];
        }
        cout<<"what about k "<<numofvectors<<endl;
        X_vectors = new double* [numofvectors+1];
        for (int C=0; C<numofvectors+1; C++) {
            X_vectors[C] = new double [Para.S];
        }
        fill_n(X_vectors[0], Para.S, 1); //for the intercept
        TF.read_vector_covariate_file (vector_covariate_stream, numofvectors, X_vectors, Para);
    }
    double** prior_cut=NULL;
    int** Y_sorted=NULL;
    if (vector_covariate_filename.length()>0) {
        prior_cut = new double* [numofvectors+1];
        for (int pc=0; pc<numofvectors+1; pc++) {
            prior_cut[pc] = new double [N-1];
        }
        double* total_dist = new double[numofvectors+1];
        fill_n(total_dist, numofvectors+1, 0);
        Y_sorted = new int* [numofvectors+1];
        for (int pc=0; pc<numofvectors+1; pc++) {
            Y_sorted[pc] = new int [N];
        }
        cout<<"Y my friend"<<endl;
        for (int pc=0; pc<numofvectors+1; pc++) {//sort each vector
            int* Y_sorted_temp = new int [2*N];
            gsl_sort_index ((size_t*)Y_sorted_temp,(const double *)X_vectors[pc],(size_t)1,(size_t)N);//only_one component
            for (int n=0; n<2*N; n+=2) {
                Y_sorted[pc][n/2]=Y_sorted_temp[n];
                cout<<Y_sorted[pc][n/2]<<" ";
            }
            cout<<" Y my friend "<<X_vectors[pc][Y_sorted[pc][0]]<<" "<<X_vectors[pc][Y_sorted[pc][1]]<<endl;
            delete[] Y_sorted_temp;
        }
        for (int pc=0; pc<numofvectors+1; pc++) {//compute the sum of the differences between the elements of the vector
            for (int n=0; n<N-1; n++) {
                if (n>=10 && n<N-10) {
                    prior_cut[pc][n]=X_vectors[pc][Y_sorted[pc][n+1]]-X_vectors[pc][Y_sorted[pc][n]];
                } else {
                    prior_cut[pc][n]=0;
                }
                total_dist[pc] += prior_cut[pc][n];
            }
        }
        for (int pc=0; pc<numofvectors+1; pc++) {//calculate prior cut for each vector
            for (int n=0; n<N-1; n++) {//N-1 total cuts
                prior_cut[pc][n]/=total_dist[pc];
            }
        }
    }
        //==================================================================
        //==================tree_covariate_file=============================
    int numoftrees=0;
    int*** X_tree_merge=NULL;
    double** X_tree_height=NULL;
    double** merge_prior=NULL;
    int** branch_countleaves=NULL;
    if (tree_covariate_filename.length()>0) {
        ifstream tree_covariate_stream (tree_covariate_filename.c_str());
        tree_covariate_stream >> numoftrees;
        X_tree_merge = new int** [numoftrees];
        for (int k=0; k<numoftrees; k++) {
            X_tree_merge[k]=new int* [Para.S-1];
            if (k<numoftrees) {
                for (int j=0; j<Para.S-1; j++) {
                    X_tree_merge[k][j]=new int [2];
                }
            }
        }
        X_tree_height = new double* [numoftrees];
        for (int k=0; k<numoftrees; k++) {
            X_tree_height[k]=new double [Para.S-1];
        }
        branch_countleaves = new int* [numoftrees];
        for (int k=0; k<numoftrees; k++) {
            branch_countleaves[k] = new int[Para.S-1];
        }
        merge_prior = new double* [numoftrees];
        for (int k=0; k<numoftrees; k++) {
            merge_prior[k]= new double [Para.S-1];
            fill_n(merge_prior[k], Para.S-1, 0);
        }
        TF.read_tree_covariate_file(tree_covariate_stream, numoftrees, X_tree_merge, X_tree_height, merge_prior, branch_countleaves, Para);
    }
    int ncol_Y=1+numofvectors+numoftrees; //+1 for the intercept
    cout<<"ncol_Y "<<ncol_Y<<endl;
    int** T_probit = new int* [M]; //tells the current statue of the col of the coavariate in Y_probit 0 if not used. 1 if it duplicate the x_covariate vector. 2 if it is binerized version. 3 for a tree.
    for (int m=0; m<M; m++) {
        T_probit[m] = new int [ncol_Y];
        fill_n(T_probit[m], ncol_Y, 0);
        T_probit[m][0]=1;
        for (int check=0; check<ncol_Y; check++) {
            cout<<T_probit[m][check]<<" ";
        }
        cout<<"probit"<<endl;
    }
        //===========================================================
        //========Y_probit, Z, and \beta=============================
    double*** Y_probit = new double** [M];
    for (int m=0; m<M; m++) {
        Y_probit[m] = new double* [ncol_Y];
        for (int pc=0; pc<ncol_Y; pc++) {
            Y_probit[m][pc] = new double [N];
            if (pc==0) {
                fill_n(Y_probit[m][pc], N, 1);
                
            } else {
                fill_n(Y_probit[m][pc], N, 0);
            }
        }
    }
    double** Z = new double* [M];
    double** beta = new double* [M];
    for (int m=0; m<M; m++) {
        Z[m]=new double[N];
        beta[m] = new double [ncol_Y];
        for (int c=0; c<ncol_Y; c++) {
            beta[m][c]= 0;
            if (c==0) {
                beta[m][c]=gsl_cdf_ugaussian_Pinv (0.01);
            }
        }
    }
    TF.sample_Z (Z, Para, p, position, X_vectors, beta, T_probit, Y_probit, ncol_Y);
        //=================================================================
        //-----------------------------Burn-in-----------------------------
    
    cout<<"Before burn in"<<endl;
    TF.compute_information_content (Para, IC, prob, w);
    if (validate) {
        TF.simulate_X (position, B, prob, backgmodel_pos, Para, p, sequences, w, refpos, seqs, rbackg); //remember
    }
    for (int sweep=0;sweep<20;sweep++) {
        TF.sample_B (sequences, position, refpos, prob, B, Para, p, w, IC, ICbg);//check2
        if (!validate) {
            TF.Update_Background_order  ( p, rbackg,  rbackmax, Para, B, sequencesMbg, seqs, positionwisebg);
        }
        TF.Update_BackgroundMr_position (sequencesMbg, B, Para, rbackg, p, backgmodel_pos, positionwisebg);
    }
    cout<<"after burn in"<<endl;
    
    double** cut = new double* [M];
    for (int m=0; m<M; m++) {
        cut[m]=new double [ncol_Y];
        fill_n(cut[m], ncol_Y, 0);
    }
        //----------------Sweeps------------------------------------------
    
    myfile<<"sweep"<<" "<<"motif"<<" "<<"nb_regions"<<" "<<"phi"<<" "<<"refpos"<<" "<<"width"<<" "<<"bg_order"<<" "<<"track_left"<<" "<<"prob_x"; //Parameters
    for (int k=0; k<ncol_Y; k++) {
        myfile<<" "<<"T_probit";
    }
    for (int k=0; k<ncol_Y; k++) {
        myfile<<" "<<"beta";
    }
    myfile<<endl;
    
    yourfile<<"sweep"<<" "<<"motif";
    for (int k=0; k<N; k++) {
        yourfile<<" "<<"position";
    }
    yourfile<<endl;
    
    ourfile<<"sweep"<<" "<<"motif"<<" "<<"width"<<" "<<"refpos" << " " << "centerpal" << " " << "npairedcolpal";
    for (int k=0; k<Para.Wmax*4; k++) {
        ourfile<<" "<<"prob";
    }
    ourfile<<endl;
    
    
    herfile<<"sweep"<<" "<<"motif"<<" "<<"phi"<<" "<<"nb_regions";
    for (int l=0; l<Para.L; l++) {
        herfile<<" "<<"pos_prob";
    }
    herfile<<endl;
    
    
    for (int sweep=0;sweep<Nsweep;sweep++) {
      //cout << "sweep " << sweep << endl;
      if (sweep%10==0) {
      //if (sweep%1==0) {
            
                //cout<<"z: "<<z<<endl;
            
            double prob_x = TF.calculate_prob_x (prob, backgmodel_pos, Para, B, sequences, position, refpos, sequencesMbg);
            
            TF.write_some_files (myfile, hisfile, ourfile, herfile, yourfile, cutfile, Para, sweep, w, refpos, prob, phi, numofvectors, K, position, pos_prob, rbackg, T_probit, beta, ncol_Y, cut, conv_saver, prob_x, center_pal, pospair_pal);
        }
        
        if (validate) {
            TF.simulate_X (position, B, prob, backgmodel_pos, Para, p, sequences, w, refpos, seqs, rbackg); //remember
        }
        
        TF.count_theta (motifs, sequences, position, B, Para, w, refpos);//check2

	TF.update_pal(motifs, Para, p, w, center_pal, pospair_pal); //PN
	  
        TF.update_theta (motifs, prob, Para, p, w, IC, IC_temp, position, refpos, prob_temp, ICbg, B,  center_pal, pospair_pal);//check2
        
        TF.compute_information_content (Para, IC, prob, w);
        
            //sample_A (prob, sequences, sequencesMbg, position, Para, p, phi, w, refpos, pos_prob, backgmodel_pos, ICbg, IC);
        
        for (int irep=0; irep<10; irep++) {
            
            TF.sample_Z (Z, Para, p, position, X_vectors, beta, T_probit, Y_probit, ncol_Y);
            
            TF.probit_dim_update (Z, Para, p, X_vectors, beta, variance_beta, T_probit, numofvectors, Y_probit, prior_cut, cut, Y_sorted, intercept_mu, intercept_variance, typeofvectors, ncol_Y, numoftrees, branch_countleaves, X_tree_merge, merge_prior);
            
            TF.sample_beta (Z, Para, p, beta, variance_beta, T_probit, ncol_Y, intercept_mu, intercept_variance, Y_probit);
        }
       
        TF.sample_A_probit (prob, sequences, sequencesMbg, position, Para, p, w, refpos, pos_prob, backgmodel_pos, ICbg, IC,beta, T_probit, numofvectors, Y_probit, X_vectors, ncol_Y, validate);
        
            //update_phi (p, phi, position, Para);
        
        if (sweep>100) {
            TF.update_w(sequences, position, prob, Para, p,backgmodel_pos,sequencesMbg, refpos, w, ICbg, IC, width_geo_prior, conv_saver, wmin_geo_prior, center_pal, pospair_pal);
        }
        
        TF.Shift_motif (p,  Para, w, pos_prob, position, refpos, sequences, sequencesMbg, prob, backgmodel_pos, ICbg, IC, conv_saver, center_pal, pospair_pal);
        
        if (sweep==1000-1) {
                //Motif_shrink(Para, IC, w, refpos, prob, IC_cut, position, conv_saver); //remember
        }
        
        TF.sample_B (sequences, position, refpos, prob, B, Para, p, w, IC, ICbg);//check2
        
        if (!validate) {
            TF.Update_Background_order  ( p, rbackg,  rbackmax, Para, B, sequencesMbg, seqs, positionwisebg);
        }
        
        TF.Update_BackgroundMr_position (sequencesMbg, B, Para, rbackg, p, backgmodel_pos, positionwisebg);
        
        
        if (TSS) {
            TF.Matrix_change (Para, position, pos_counter); //check1
            
            TF.count_Q (K, D, Q, Para, pos_counter); //check1
            
            TF.sample_D (D, temp_D, Para, K, p, Q, temp_Q, pos_counter, point_detector); //check1
            
            TF.update_K (D, temp_D, Para, K, p, Q, temp_Q, pos_counter, point_detector, geo_prior_breakpoints); //check1
            
            TF.update_Gamma (D, Q, Gamma, Para, p, K); //check1
            
            TF.cal_pos_prob (D, K, Gamma, Para, pos_prob, point_detector); //check1
        }
        
        if (sweep>100) {
            TF.update_refpos(D, Para, K, p, refpos, position, w, point_detector, pos_prob, pos_counter);
        }
        
        if (sweep%100==0) {
            cout<<"sweep "<<sweep<<endl;
        }
        
    }//sweeps_end_here
    
    for (int m=0; m<M; m++) {
        cout<<m<<" "<<w[m]<<endl;
        for(int j=0; j<N; j++) {
            if (position[m][j]<Para.L) {
                cout<<j<<' '<<position[m][j]-refpos[m]<<' ';
                for(int r=0;r<w[m];r++) {
                    if (0<=position[m][j]-refpos[m]+r && position[m][j]-refpos[m]+r<Para.L) {
                        cout<<sequences[j][position[m][j]-refpos[m]+r];
                    }
                }
                cout<<endl;
            }
        }
    }
        //============================================================================
    
    for (int j=0;j<M;j++) {
        delete [] position[j];
        delete [] D[j];
        delete [] temp_D[j];
        delete [] Q[j];
        delete [] temp_Q[j];
        delete [] pos_counter[j];
        delete [] pos_prob[j];
        delete [] Gamma[j];
        delete [] point_detector[j];
        delete [] IC[j];
        delete [] IC_temp[j];
    }
    delete [] position;
    delete [] D;
    delete [] temp_D;
    delete [] Q;
    delete [] temp_Q;
    delete [] pos_counter;
    delete [] pos_prob;
    delete [] Gamma;
    delete [] point_detector;
    delete [] IC;
    delete [] IC_temp;
    
    for (int j=0; j<N; j++) {
        delete [] B[j];
    }
    delete [] B;
    
    for (int j=0; j<M; j++) {
        for (int k=0; k<wmax; k++) {
            delete [] prob[j][k];
            delete [] prob_temp[j][k];
            delete []  motifs[j][k];
        }
        delete [] prob[j];
        delete [] prob_temp[j];
        delete [] motifs[j];
    }
    delete [] prob;
    delete [] prob_temp;
    delete [] motifs;
    
    for (int x=0; x<Para.L; x++) {
        delete [] backgmodel_pos[x];
    }
    delete [] backgmodel_pos;
    delete seqs; // PN add
    gsl_rng_free(p); // PN add
    return 0;
}
